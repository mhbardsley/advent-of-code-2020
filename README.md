# Advent of Code 2020

Code for the advent of code 2020.

## Journal
### Day 1
- Trivial
### Day 2
- Trivial
### Day 3
- Trivial
### Day 4
- Trivial
### Day 5
- Map to a binary string, where B goes to 1, R goes to 1
- Create a type that holds the row int, then one for the seat int
- For part 2, it should be the case that my seat is not a member of the list, but my seat + 1 is and my seat - 1 is
### Day 6
- Part 1 should be trivial
- Part 2, instead construct a set of their responses and take the intersection
### Day 7
- Tougher, but very achievable with this language
- Create type with following fields
	- Bag name
	- List of what it contains
- Create contains type with following
	- Number
	- Name of bag
- For now, just create that and convert to it using string separation
- The predicate `find_bags` gets a list of bags that are dependant on the other
- Need to call it immediate bags, and have `find_bags` predicate that wil take list of strings
- It's much easier than I thought - just find the ones that eventually contain it with recursive calls
- Part 2
- Fold over the same function for its dependencies, multiplying the result by how many bags there are of it
- Each bag call should return the amomunt of them plus the amount of them * total of ones inside
### Day 8
- Trivial
### Day 9
- Part 2, encryption weakness
- First, find contiguous set
- Need to create nondeterministic "continguous powerset" predicate for this
### Day 10
- Try and tailor this one more as a representation of logical problems, as opposed to trying to find a method to solve it
- Consider easier cases
### Day 11
- Have a predicate that takes a column, row, map, converts it on certain rules
- Have a predicate that takes a mapping, creates new one, and tests to determine what to return
- Need to deal with edge cases for `get_adjacents` predicate
- For the remaining ones, try it on the test input first
### Day 12
- Type to hold current ship information
	- Amount east
	- Amount north
	- Current bearing
- This can then be used to change state with a function that takes this with an instruction
- Fold this over the list of instructions
### Day 13
- Need to solve simultaneous equations
- Smallest value X such that
- For all items in the list L
	- L[0] divides X
	- k * L[1] = X + 1
	- Generally, there's some k s.t.
		- k * L[x] - x = X
	- aL[0]bL[1]cL[2].. = X(X+1)(X+2)
	- X(X+1)...(X + x) is divisible by L[0]L[1]L[2]...L[x]
- Instead of tackling the bigger problem, it's probably easier to consider building up the solution
- Start with the biggest problem, i.e. the biggest number
- Sorted list of services with their associated
- Need to use heuristics
- Try a small example - [17, x, 13, 19], X is 3417
- How do we get to that X?
- System of congruences might work
- X(X+2)(X+3) = 0 mod 13
- 0, 10, 11
- 0, 14, 15
- 0, 16, 17
- Overthinking it. Maybe come back to this
- Having come back to this, the best option would be to implement the Chinese remainder theorem
- Go through it as an example
	- [17, x, 13, 19]
	- These are all coprime (is the same case for the real inputs)
	- System of equations:
		- x = 0 mod 17
		- x = 11 mod 13
		- x = 16 mod 19
	- Using Bezout's identity
		- m1*17 + m2*13 = 1
	- Using extended Euclidean algorithm
		- Just code it and verify it works
	- m1 = -3, m2 = 4
	- x = a1m2n2 + a2m1n1 = 0 + 11 * -3 * 17 = 
- Action plan
	- Predicate to convert the initial list to a pair of numbers with their mod number
	- Predicate to enforce extended Euclidean algorithm to get Bezout coefficients
	- Predicate to solve system of equations
- This doesn't work due to the repetitions
- Instead use a linear Diophantine system
	- x = x1(13)
	- x = -3 + x2(13)
	- x = -13 + x3(997)
- The reason it was initially failing was the system could not hold the numbers as ints, so had to use `integer`
### Day 14
- Have a map representing memory, initially empty
- Predicate for converting current value to binary list
- Predicate for masking a binary list
- Predicate for converting binary list to current value (might play in with other direction)
- Predicate to go through list, either setting the mask or setting memory
- Predicate to get value obtained from memory
- Bitwise operations exist in Mercury - instead, just convert the mask to a value
- Never mind, Bitwise operations won't work - back to plan A
- Code for part 1 is messy, so might redo for part 2
- Part 2
	- Nondet pred to get an address from a mask
	- Nondet pred to mask an address
	- Pred to parse an instruction
### Day 15
- Try to modularise the issue as you go through it
- For part 2, use a [hash]map to store the most recent indices of given numbers
### Day 18
- S --> Expr Rest | nothing
- Expr --> ( Expr Rest ) | LIT
- Rest --> Op Expr Rest | nothing

- For part 2
- Term --> LIT | ( Mult )
- Mult --> Add Mult'
- Mult' --> * Add Mult' | / Add Mult' | epsilon
- Add --> Term Add' | epsilon
- Add' --> + Term Add' | - Term Add'
### Day 21
- Part 1 to identify which ingredients cannot contain an allergen
- Let's see if we can purely use logic for this
- Have a nondeterministic parser
- A mapping of allergen to exactly one ingredient
- A mapping of an ingredient to zero/one allergens
- Initially, no mapping
- On each parse:
	- Ensure mappings are consistent
	- i.e., if allergen already has mapping, check it is still there
	- If no mapping, put it on an ingredient that doesn't already have it
- This will generate matchings, from which we can reduce to those that never get a matching
- Might get a bit too complex for large input
- Works but is too complex
- Instead maintain possible allergen-ingredient matchings
- Update them
- Count the ingredients never suggested in a matching
- For part 2
	- Repetitively select allergen with lowest possible ingredients
	- Select from there, continue
### Day 22
- Process stream to get the two decks
- Iterate by comparing top vals, until one doesn't have a top val
- Then calculate with the other deck
- For part 2, need to pass in a game history as a set of list pairs
- Add in a check for this
- Game needs to return bool identity of winner as well as deck for sub-games
### Day 23
- Do 100 times:
	- Take first four elements, and the rest
	- Get the value of the next one
	- Take while not equal to the destination element
	- Insert the three 
	- Shift elements left
- Shift right by 100 mod length
- Convert to int
- A need for efficiency changes
	- A map to hold items in initial order
	- Get the fourth item, change such that first points to fourth
	- Get value in rest, point third to what it points to (and vice versa), value in rest to first
	- Shift along by moving start pointer to second and last to first (dedicated last to first)
### Day 24
- Keep a set of coordinates, initially empty
- Evaluate each line to a pair of coordinates in 2D space
- In set already => out of set, vice versa
- Part 2
- Take the set of blacks firstly to create a subset
- Then, create the set of possible new blacks by taking the X values and Y values from lowest - 2 to highest + 2, complement with set of blacks
- A new black is one s.t.
	- It has coords (X, Y) and is scoped to possible new blacks
	- There are exactly two neighbouring black tiles (X and Y total difference is 2)

### Day 25
- A predicate to carry out the handshake
- A predicate to determine the loop size 
- Probably possible with modular exponentiation
