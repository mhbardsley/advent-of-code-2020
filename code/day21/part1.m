/* Advent of Code - Day 21, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, maybe, list, string, pair, set, 
	require, solutions, int, map, assoc_list.

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	to_pairs(StringRep, Pairs),
	get_matchings(Pairs, Matchings),
	get_ingredients(Pairs, Ingredients),
	reduce_matchings(Ingredients, Matchings, EmptyIngredients),
	count_occurrences(Pairs, EmptyIngredients, Result).

:- pred to_pairs(list(string)::in, list(pair(list(string), list(string)))::out)
	is det.
to_pairs(StringRep, Pairs) :-
	map(to_pair, StringRep, Pairs).

:- pred to_pair(string::in, pair(list(string), list(string))::out) is det.
to_pair(Line, Pair) :-
	StrPair = split_at_string(" (contains ", Line),
	( if
		StrPair = [Ingredients, Allergens]
	then
		process_ingredients(Ingredients, IngList),
		process_allergens(Allergens, AllList),
		Pair = pair(IngList, AllList)
	else
		error("Issue splitting a string when processing stream 
			(to_pair)")
	).

:- pred process_ingredients(string::in, list(string)::out) is det.
process_ingredients(Ingredients, IngList) :-
	IngList = words(Ingredients).

:- pred process_allergens(string::in, list(string)::out) is det.
process_allergens(Allergens, AllList) :-
	( if
		remove_suffix(Allergens, ")", WithoutParen)
	then
		AllList = split_at_string(", ", WithoutParen)
	else
		error("Issue removing right parenthesis (process_allergens)")
	).

:- pred get_matchings(list(pair(list(string), list(string)))::in,
	assoc_list(string, set(string))::out) is det.
get_matchings(Pairs, Matchings) :-
	foldl(get_matching, Pairs, init, MatchingsMap),
	to_assoc_list(MatchingsMap, Matchings).

:- pred get_matching(pair(list(string), list(string))::in,
	map(string, set(string))::in, map(string, set(string))::out) is det.
get_matching(Pair, AllIn, AllOut) :-
	fst(Pair, Ingredients),
	snd(Pair, Allergens),
	foldl(get_allergen(Ingredients), Allergens, AllIn, AllOut).

:- pred get_allergen(list(string)::in, string::in,
	map(string, set(string))::in, map(string, set(string))::out) is det.
get_allergen(Ingredients, Allergen, AllIn, AllOut) :-
	( if
		search(AllIn, Allergen, PossibleIngredients)
	then
		list_to_set(Ingredients, IngSet),
		intersect(PossibleIngredients, IngSet, ReducedIngredients),
		set(Allergen, ReducedIngredients, AllIn, AllOut)
	else
		list_to_set(Ingredients, IngSet),
		set(Allergen, IngSet, AllIn, AllOut)
	).

:- pred get_ingredients(list(pair(list(string), list(string)))::in,
	set(string)::out) is det.
get_ingredients(Pairs, Ingredients) :-
	foldl(get_pair_ingredients, Pairs, init, Ingredients).

:- pred get_pair_ingredients(pair(list(string), list(string))::in,
	set(string)::in, set(string)::out) is det.
get_pair_ingredients(Pair, IngIn, IngOut) :-
	fst(Pair, Ingredients),
	list_to_set(Ingredients, IngSet),
	union(IngIn, IngSet, IngOut).

:- pred reduce_matchings(set(string)::in, 
	assoc_list(string, set(string))::in, set(string)::out) is det.
reduce_matchings(Ingredients, Matchings, IngsNotPresent) :-
	foldl(reduce_matching, Matchings, Ingredients, IngsNotPresent).

:- pred reduce_matching(string::in, set(string)::in, set(string)::in,
	set(string)::out) is det.
reduce_matching(_, PossibleIngredients, IngIn, IngOut) :-
	difference(IngIn, PossibleIngredients, IngOut).

:- pred count_occurrences(list(pair(list(string), list(string)))::in,
	set(string)::in, int::out) is det.
count_occurrences(Pairs, IngsNotPresent, Result) :-
	foldl(count_occurrences_pair(IngsNotPresent), Pairs, 0, Result).

:- pred count_occurrences_pair(set(string)::in,
	pair(list(string), list(string))::in, int::in, int::out) is det.
count_occurrences_pair(IngsNotPresent, Pair, IntIn, IntOut) :-
	Ingredients = fst(Pair),
	list_to_set(Ingredients, IngSet),
	intersect(IngSet, IngsNotPresent, Occurrences),
	count(Occurrences, NoOfOccurrences),
	IntOut = IntIn + NoOfOccurrences.
