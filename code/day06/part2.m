/* Advent of Code - Day 6, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, char, set, int.

:- pred normalise(list(string)::in, list(set(char))::in, list(list(set(char)))::out) is det.
normalise(Initial, SetOfPrevious, Result) :-
	( if
	  	Head = head(Initial),
		Tail = tail(Initial)
	then
		( if
		  	is_empty(Head)
		then
			normalise(Tail, [], Rest),
			Result = [SetOfPrevious | Rest]
		else
			to_char_list(Head, CharList),
			list_to_set(CharList, CharSet),
			NewCharList = [CharSet | SetOfPrevious],
			normalise(Tail, NewCharList, Result)
		)
	else
		Result = [SetOfPrevious]
	).

:- pred accumulated_length(list(char)::in, int::in, int::out) is det.
accumulated_length(ListOfChars, Int, Result) :-
	length(ListOfChars, ListLength),
	Result = Int + ListLength.

main(!IO) :-
	process_stream(!IO, Initial),
	normalise(Initial, [], Normalised),
	SetResult = list.map(intersect_list, Normalised),
	list.map(to_sorted_list, SetResult, ListResult),
	foldl(accumulated_length, ListResult, 0, Result),
	io.print(Result, !IO),
	io.nl(!IO).
