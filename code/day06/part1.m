/* Advent of Code - Day 6, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int.

:- pred normalise(list(string)::in, string::in, list(string)::out) is det.
normalise(Initial, AppendString, Result) :-
	( if
	  	Head = head(Initial),
		Tail = tail(Initial)
	then
		( if
		  	is_empty(Head)
		then
			normalise(Tail, "", Rest),
			Result = [AppendString | Rest]
		else
			append(AppendString, Head, NewAppend),
			normalise(Tail, NewAppend, Result)
		)
	else
		Result = [AppendString]
	).

:- pred string_set(string::in, string::out) is det.
string_set(InString, Result) :-
	to_char_list(InString, CharList),
	remove_dups(CharList, CharSet),
	from_char_list(CharSet, Result).

:- pred make_unique(list(string)::in, list(string)::out) is det.
make_unique(InList, Result) :-
	map(string_set, InList, Result).

:- pred accumulated_length(string::in, int::in, int::out) is det.
accumulated_length(String, Int, Result) :-
	length(String, StringLength),
	Result = Int + StringLength.

main(!IO) :-
	process_stream(!IO, Initial),
	normalise(Initial, "", Normalised),
	make_unique(Normalised, NormalSet),
	foldl(accumulated_length, NormalSet, 0, Result),
	io.print(Result, !IO),
	io.nl(!IO).
