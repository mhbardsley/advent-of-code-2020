/* Advent of Code: Day 7, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, int, string, list, solutions.

:- type bag
	--->	bag(
			bag_name :: string,
			inside :: list(contains)
		).

:- type contains
	--->	contains(
			bag_no :: int,
			  name :: string
			).

:- pred to_contains(string::in, contains::out) is semidet.
to_contains(String, Result) :-
	ListOf = words(String),
	Number = head(ListOf),
	DescriptionPlusBag = tail(ListOf),
	split_last(DescriptionPlusBag, Description, _),
	to_int(Number, BagNo),
	Name = join_list(" ", Description),
	Result = contains(BagNo, Name).

:- pred parser(string::in, bag::out) is semidet.
parser(InString, Bag) :-
	remove_suffix(InString, ".", String),
	sub_string_search(String, " bags contain ", SP1),
	split(String, SP1, Name, Rest),
	remove_prefix(" bags contain ", Rest, Inside),
	ContainsString = split_at_string(", ", Inside),
	filter_map(to_contains, ContainsString, Contains),
	Bag = bag(Name, Contains).

:- pred get_bags(list(bag)::in, contains::in, int::in, int::out) is nondet.
get_bags(Mappings, contains(NumberOfIt, Name), AccumulatorVal, Result) :-
	Bag = bag(Name, BagsItContains),
	member(Bag, Mappings),
	foldl(get_bags(Mappings), BagsItContains, 0, SubResult),
	Result = AccumulatorVal + NumberOfIt + NumberOfIt * SubResult.

main(!IO) :-
	process_stream(!IO, Initial),
	filter_map(parser, Initial, Mappings),
	solutions(get_bags(Mappings, contains(1, "shiny gold"), -1), Result),
	io.print(Result, !IO),
	io.nl(!IO).
