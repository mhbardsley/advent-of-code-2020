/* Advent of Code: Day 7, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, int, string, list, solutions.

:- type bag
	--->	bag(
			bag_name :: string,
			inside :: list(contains)
		).

:- type contains
	--->	contains(
			bag_no :: int,
			  name :: string
			).

:- pred to_contains(string::in, contains::out) is semidet.
to_contains(String, Result) :-
	ListOf = words(String),
	Number = head(ListOf),
	DescriptionPlusBag = tail(ListOf),
	split_last(DescriptionPlusBag, Description, _),
	to_int(Number, BagNo),
	Name = join_list(" ", Description),
	Result = contains(BagNo, Name).

:- pred parser(string::in, bag::out) is semidet.
parser(InString, Bag) :-
	remove_suffix(InString, ".", String),
	sub_string_search(String, " bags contain ", SP1),
	split(String, SP1, Name, Rest),
	remove_prefix(" bags contain ", Rest, Inside),
	ContainsString = split_at_string(", ", Inside),
	filter_map(to_contains, ContainsString, Contains),
	Bag = bag(Name, Contains).

:- pred leads_to(string::in, list(bag)::in, bag::in, string::out) is semidet.
leads_to(String, Mappings, bag(Name, Inside), Name) :-
	some [SomeName]
	(
		member(contains(_, SomeName), Inside),
		(
			SomeName = String
		;
			SomeBag = bag(SomeName, _),
			member(SomeBag, Mappings),
			leads_to(String, Mappings, SomeBag, _)
		)
	).

main(!IO) :-
	process_stream(!IO, Initial),
	filter_map(parser, Initial, Mappings),
	filter_map(leads_to("shiny gold", Mappings), Mappings, Dependants),
	length(Dependants, Result),
	io.print(Result, !IO),
	io.nl(!IO).
