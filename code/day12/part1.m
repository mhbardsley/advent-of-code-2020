/* Advent of Code - Day 12, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, string, list, int.

:- type location
	--->	location(
			east :: int,
		       north :: int,
		     bearing :: int
		).

:- pred update_location(string::in, location::in, location::out) is det.
update_location(Instruction, location(E, N, B), Result) :-
	split(Instruction, 1, Operator, Operand),
	OperandInt = det_to_int(Operand),
	( if
	  	Operator = "N"
	then
		Result = location(E, N + OperandInt, B)
	else if
		Operator = "S"
	then
		Result = location(E, N - OperandInt, B)
	else if
		Operator = "E"
	then
		Result = location(E + OperandInt, N, B)
	else if
		Operator = "W"
	then
		Result = location(E - OperandInt, N, B)
	else if
		Operator = "L"
	then
		Result = location(E, N, (B - OperandInt) mod 360)
	else if
		Operator = "R"
	then
		Result = location(E, N, (B + OperandInt) mod 360)
	else
		% assumed to be a move forward in direction
		( if
		  	B = 0
		then
			Result = location(E, N + OperandInt, B)
		else if
			B = 90
		then
			Result = location(E + OperandInt, N, B)
		else if
			B = 180
		then
			Result = location(E, N - OperandInt, B)
		else
			% assumed to be west
			Result = location(E - OperandInt, N, B)
		)
	).

:- pred manhattan_of(location::in, int::out) is det.
manhattan_of(location(E, N, _), Result) :-
	Result = abs(E) + abs(N).

:- pred get_distance(list(string)::in, int::out) is det.
get_distance(Instructions, Result) :-
	foldl(update_location, Instructions, location(0, 0, 90), Temp),
	manhattan_of(Temp, Result).

main(!IO) :-
	process_stream(!IO, Initial),
	get_distance(Initial, Result),
	io.print(Result, !IO),
	io.nl(!IO).
