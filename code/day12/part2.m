/* Advent of Code - Day 12, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, string, list, int.

:- type location
	--->	location(
			east :: int,
		       north :: int
		).

:- type double_locations
	--->
		double_locations(
			ship :: location,
			waypoint :: location
		).

:- pred update_locations(string::in, double_locations::in, double_locations::out) is det.
update_locations(Instruction, double_locations(ShipIn, WPIn), double_locations(ShipOut, WPOut)) :-
	ShipIn = location(SE, SN),
	WPIn = location(WE, WN),
	split(Instruction, 1, Operator, Operand),
	OperandInt = det_to_int(Operand),
	( if
	  	Operator = "N"
	then
		WPOut = location(WE, WN + OperandInt),
		ShipOut = ShipIn
	else if
		Operator = "S"
	then
		WPOut = location(WE, WN - OperandInt),
		ShipOut = ShipIn
	else if
		Operator = "E"
	then
		WPOut = location(WE + OperandInt, WN),
		ShipOut = ShipIn
	else if
		Operator = "W"
	then
		WPOut = location(WE - OperandInt, WN),
		ShipOut = ShipIn
	else if
		Operator = "L"
	then
		( if 
			OperandInt mod 360 = 0
		then
			WPOut = WPIn
		else if
			OperandInt mod 360 = 90
		then
			WPOut = location(-WN, WE)
		else if
			OperandInt mod 360 = 180
		then
			WPOut = location(-WE, -WN)
		else
			WPOut = location(WN, -WE)
		),
		ShipOut = ShipIn
	else if
		Operator = "R"
	then
		( if 
			OperandInt mod 360 = 0
		then
			WPOut = WPIn
		else if
			OperandInt mod 360 = 270
		then
			WPOut = location(-WN, WE)
		else if
			OperandInt mod 360 = 180
		then
			WPOut = location(-WE, -WN)
		else
			WPOut = location(WN, -WE)
		),
		ShipOut = ShipIn
	else
		ShipOut = location(SE + OperandInt * WE, SN + OperandInt * WN),
		WPOut = WPIn
	).

:- pred manhattan_of(location::in, int::out) is det.
manhattan_of(location(E, N), Result) :-
	Result = abs(E) + abs(N).

:- pred get_distance(list(string)::in, int::out) is det.
get_distance(Instructions, Result) :-
	foldl(update_locations, Instructions, double_locations(location(0, 0), location(10, 1)), double_locations(Temp, _)),
	manhattan_of(Temp, Result).

main(!IO) :-
	process_stream(!IO, Initial),
	get_distance(Initial, Result),
	io.print(Result, !IO),
	io.nl(!IO).
