/* Input Processor
* Author: Matthew Bardsley
*/
:- module input_processor.
:- interface.
:- import_module io, list, string.
:- pred process_stream(io::di, io::uo, list(string)::out) is det.
:- pred process_line(io.text_input_stream::in, list(string)::out, io::di, io::uo) is det.
:- implementation.

process_line(Stream, Xs, !IO) :-
	io.read_line_as_string(Stream, Result, !IO),
	( if 
		Result = io.ok(String),
		X = chomp(String)
	then
		process_line(Stream, Rest, !IO),
		Xs = [X | Rest]
	else
		Xs = []
	).

process_stream(!IO, N):-
	io.open_input("input", ResStream, !IO),
	( if
		ResStream = io.ok(Stream)
	then
		process_line(Stream, Result, !IO),
		io.close_input(Stream, !IO),
		N = Result
	else
		io.write_string("Issue with input stream.", !IO),
		io.nl(!IO),
		N = []
	).

:- end_module input_processor.
