/* Advent of Code 2020 - Day 17, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, char, int, solutions.

:- type coords
	--->	coords(
			x :: int,
			y :: int,
			z :: int,
			w :: int
		).

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	to_active_states(StringRep, 0, ActiveStates),
	six_cycles(ActiveStates, FinalStates),
	count_actives(FinalStates, Result).

:- pred to_active_states(list(string)::in, int::in, list(coords)::out) is det.
to_active_states(StringRep, Y, Coords) :-
	( if
		StringRep = [X | Xs]
	then
		to_active_state(X, Y, SomeCoords),
		to_active_states(Xs, Y + 1, OtherCoords),
		Coords = SomeCoords ++ OtherCoords
	else
		Coords = []
	).

:- pred to_active_state(string::in, int::in, list(coords)::out) is det.
to_active_state(InString, Y, Coords) :-
	to_char_list(InString, CharList),
	charlist_active_state(CharList, 0, Y, Coords).

:- pred charlist_active_state(list(char)::in, int::in, int::in, list(coords)::out) is det.
charlist_active_state(CharList, X, Y, Coords) :-
	( if
		CharList = [Char | Rest]
	then
		( if
			Char = '#'
		then
			charlist_active_state(Rest, X + 1, Y, OtherCoords),
			Coords = [coords(X, Y, 0, 0) | OtherCoords]
		else
			charlist_active_state(Rest, X + 1, Y, Coords)
		)
	else
		Coords = []
	).

:- pred six_cycles(list(coords)::in, list(coords)::out) is det.
six_cycles(CoordsIn, CoordsOut) :-
	run_six_cycles(CoordsIn, 0, CoordsOut).

:- pred run_six_cycles(list(coords)::in, int::in, list(coords)::out) is det.
run_six_cycles(CoordsIn, CycleNo, CoordsOut) :-
/*	trace [io(!IO)]
	(
		io.print(CoordsIn, !IO),
		io.nl(!IO),
		io.nl(!IO)
	),
*/
	( if
		CycleNo < 6
	then
		run_cycle(CoordsIn, [], CoordsIn, NewCoords),
		run_six_cycles(NewCoords, CycleNo + 1, CoordsOut)
	else
		CoordsOut = CoordsIn
	).

:- pred run_cycle(list(coords)::in, list(coords)::in, list(coords)::in, list(coords)::out) is det.
run_cycle(CoordsIn, CurrentNewCoords, Actives, CoordsOut) :-
	( if
		CoordsIn = [Coords | Rest]
	then
		solutions(nearby_active(Coords, CurrentNewCoords, Actives), NewCoords),
		run_cycle(Rest, CurrentNewCoords ++ NewCoords, Actives, CoordsOut)
	else
		CoordsOut = CurrentNewCoords
	).

:- pred nearby_active(coords::in, list(coords)::in, list(coords)::in, coords::out) is nondet.
nearby_active(Coords, CurrentNewCoords, Actives, ActiveNeighbour) :-
	Coords = coords(X, Y, Z, W),
	ActiveNeighbour = coords(NewX, NewY, NewZ, NewW),
	member(NewX, X - 1 .. X + 1),
	member(NewY, Y - 1 .. Y + 1),
	member(NewZ, Z - 1 .. Z + 1),
	member(NewW, W - 1 .. W + 1),
	not member(coords(NewX, NewY, NewZ, NewW), CurrentNewCoords),
	should_be_active(coords(NewX, NewY, NewZ, NewW), Actives).

:- pred should_be_active(coords::in, list(coords)::in) is semidet.
should_be_active(Coords, Actives) :-
	get_active_neighbours(Coords, Actives, NoOfActives),
	( if
		member(Coords, Actives)
	then
		NoOfActives = 2 ; NoOfActives = 3
	else
		NoOfActives = 3
	).

:- pred get_active_neighbours(coords::in, list(coords)::in, int::out) is det.
get_active_neighbours(Coords, Actives, Result) :-
	solutions(get_active_neighbour(Coords, Actives), ActiveNeighbours),
	length(ActiveNeighbours, Result).

:- pred get_active_neighbour(coords::in, list(coords)::in, coords::out) is nondet.
get_active_neighbour(Coords, Actives, ActiveNeighbour) :-
	Coords = coords(X, Y, Z, W),
	ActiveNeighbour = coords(NeighbourX, NeighbourY, NeighbourZ, NeighbourW),
	member(ActiveNeighbour, Actives),
	member(NeighbourX, X - 1 .. X + 1),
	member(NeighbourY, Y - 1 .. Y + 1),
	member(NeighbourZ, Z - 1 .. Z + 1),
	member(NeighbourW, W - 1 .. W + 1),
	Coords \= ActiveNeighbour.

:- pred count_actives(list(coords)::in, int::out) is det.
count_actives(Cubes, Result) :-
	length(Cubes, Result).
