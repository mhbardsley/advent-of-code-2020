/* Advent of Code: Day 2, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module char, int, string, list, solutions.

:- func count(char, string) = int.
count(C, S) = N :-
	( if
		first_char(S, OtherC, Rest)
	then
		R = count(C, Rest),
		( if
			OtherC = C
		then
			N = R + 1
		else
			N = R
		)
	else
		N = 0
	).

:- pred conforms(string::in, int::out) is nondet.
conforms(String, Result) :-
	append(S1, "-", S2),
	nondet_append(S2, S3, S4),
	append(S4, " ", S5),
	nondet_append(S5, S6, S7),
	append(S7, ": ", S8),
	nondet_append(S8, S9, S10),
	append(S10, "\n", String),
	to_int(S1, I1),
	to_int(S3, I2),
	first_char(S6, C1, _),
	N = count(C1, S9),
	( if 
		N >= I1,
		N =< I2
	then
		Result = 1
	else
		Result = 0
	).

:- pred process_stream(io.text_input_stream::in, int::out, io::di, io::uo) is det.
process_stream(Stream, Xs, !IO) :-
	io.read_line_as_string(Stream, Result, !IO),
	( if 
		Result = ok(String),
		solutions(conforms(String), Solutions),
		NoToAdd = head(Solutions)
	then
		process_stream(Stream, Rest, !IO),
		Xs = NoToAdd + Rest
	else
		Xs = 0
	).

main(!IO) :-
	io.open_input("input", ResStream, !IO),
	( if
		ResStream = ok(Stream)
	then
		process_stream(Stream, Result, !IO),
		io.print(Result, !IO)
	else
		io.write_string("Issue with input stream.", !IO)
	),
	io.nl(!IO).
