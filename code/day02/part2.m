/* Advent of Code: Day 2, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module char, int, string, list, solutions.

:- pred conforms(string::in, int::out) is nondet.
conforms(String, Result) :-
	append(S1, "-", S2),
	nondet_append(S2, S3, S4),
	append(S4, " ", S5),
	nondet_append(S5, S6, S7),
	append(S7, ": ", S8),
	nondet_append(S8, S9, S10),
	append(S10, "\n", String),
	to_int(S1, I1),
	to_int(S3, I2),
	first_char(S6, C1, _),
	Index1 = I1 - 1,
	Index2 = I2 - 1,
	Target = to_char_list(S9),
	( if
		member_index0(C1, Target, Index1) <=> member_index0(C1, Target, Index2)
	then
		Result = 0
	else
		Result = 1
	).

:- pred process_stream(io.text_input_stream::in, int::out, io::di, io::uo) is det.
process_stream(Stream, Xs, !IO) :-
	io.read_line_as_string(Stream, Result, !IO),
	( if 
		Result = ok(String),
		solutions(conforms(String), Solutions),
		NoToAdd = head(Solutions)
	then
		process_stream(Stream, Rest, !IO),
		Xs = NoToAdd + Rest
	else
		Xs = 0
	).

main(!IO) :-
	io.open_input("input", ResStream, !IO),
	( if
		ResStream = ok(Stream)
	then
		process_stream(Stream, Result, !IO),
		io.print(Result, !IO)
	else
		io.write_string("Issue with input stream.", !IO)
	),
	io.nl(!IO).
