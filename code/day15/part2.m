/* Advent of Code - Day 15, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module list, int, hash_table.

main(!IO) :-
	io.print(Result, !IO),
	io.nl(!IO),
	initialise_ht(HT),
	initialise_game([6,3,15,13,1,0], 1, HT, SeedVal, SeedIndex, SeedHT),
	complete_game(SeedVal, SeedIndex, SeedHT, Result).

:- pred initialise_ht(hash_table(int, int)::hash_table_uo) is det.
initialise_ht(Result) :-
	Result = init_default(pred(X::in, Y::out) is det :- Y = X).

:- pred initialise_game(list(int)::in, int::in, hash_table(int, int)::hash_table_di, int::out, int::out, hash_table(int, int)::hash_table_uo) is det.
initialise_game(Sequence, Index, HT, SeedVal, SeedIndex, SeedHT) :-
	( if
	  	Sequence = [X | Xs]
	then
		set(X, Index, HT, NewHT),
		( if
		  	Xs = []
		then
			SeedVal = X,
			SeedIndex = Index,
			SeedHT = NewHT
		else
			initialise_game(Xs, Index + 1, NewHT, SeedVal, SeedIndex, SeedHT)
		)
	else
		SeedVal = 0,
		SeedIndex = Index,
		SeedHT = copy(HT)
	).

:- pred complete_game(int::in, int::in, hash_table(int, int)::hash_table_ui, int::out) is det.
complete_game(LastVal, Index, HT, Result) :-
/*	trace [io(!IO)] 
	(
	 	io.print(LastVal, !IO),
		io.nl(!IO),
		io.print(Result, !IO),
		io.nl(!IO)
	),
*/
	( if
		Index < 30000000
	then
		complete_game(NextVal, Index + 1, NewHT, Result),
		next_val(LastVal, Index, HT, HT, NextVal, NewHT)
	else
		Result = LastVal
	).

:- pred next_val(int::in, int::in, hash_table(int, int)::hash_table_ui,   hash_table(int, int)::hash_table_di, int::out, hash_table(int, int)::hash_table_uo) is det.
next_val(LastNumber, Index, HT, HTCopy, Result, NewHT) :-
/*	trace [io(!IO)] 
	(
	 	io.print(Sequence, !IO),
		io.nl(!IO),
		io.print(Result, !IO),
		io.nl(!IO)
	),
*/
	( if
		search(HT, LastNumber, Pos)
	then
		Result = Difference,
		Difference = Index - Pos
	else
		Result = 0
	),
	set(LastNumber, Index, HTCopy, NewHT).
