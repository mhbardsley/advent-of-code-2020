/* Advent of Code - Day 15, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module list, int.

main(!IO) :-
	io.print(Result, !IO),
	io.nl(!IO),
	det_last(GameVals, Result),
	complete_game([6,3,15,13,1,0], GameVals).

:- pred complete_game(list(int)::in, list(int)::out) is det.
complete_game(Sequence, Result) :-
/*	trace [io(!IO)] 
	(
	 	io.print(Sequence, !IO),
		io.nl(!IO),
		io.print(Result, !IO),
		io.nl(!IO)
	),
*/
	length(Sequence, SeqLength),
	( if
		SeqLength < 2020
	then
		NewSequence = Sequence ++ [NextVal],
		complete_game(NewSequence, Result),
		next_val(Sequence, NextVal)
	else
		Result = Sequence
	).

:- pred next_val(list(int)::in, int::out) is det.
next_val(Sequence, Result) :-
/*	trace [io(!IO)] 
	(
	 	io.print(Sequence, !IO),
		io.nl(!IO),
		io.print(Result, !IO),
		io.nl(!IO)
	),
*/
	det_split_last(Sequence, PreLast, LastNumber),
	member_indexes0(LastNumber, PreLast, Indexes),
	( if
		Indexes = []
	then
		Result = 0
	else
		Result = Difference,
		det_last(Indexes, LastIndex),
		length(PreLast, Length),
		Difference = Length - LastIndex
	).
