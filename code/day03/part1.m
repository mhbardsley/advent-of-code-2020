/* Advent of Code: Day 3, Part 1
* Author: Matthew Bardsley
*/
:- module part1.
:- interface.
:- use_module io.
:- pred main(io.io::di, io.io::uo) is det.
:- implementation.
:- use_module list, string, char, int.

:- pred process_stream(io.text_input_stream::in, list.list(list.list(int))::out, io.io::di, io.io::uo) is det.
process_stream(Stream, Xs, !IO) :-
	io.read_line_as_string(Stream, Result, !IO),
	( if 
		Result = io.ok(String),
		ListOfChars = string.to_char_list(string.chomp(String)),
		X = list.map(char.to_int, ListOfChars)
	then
		process_stream(Stream, Rest, !IO),
		Xs = list.[X | Rest]
	else
		Xs = list.[]
	).

:- func no_of_trees(int, int, list.list(list.list(int))) = int.
no_of_trees(DownIndex, RightIndex, ListOfInts) = N :-
	( if
	  	list.index0(ListOfInts, DownIndex, FurtherInts),
		SpecificIndex = int.mod(RightIndex, list.length(FurtherInts)),
		list.index0(FurtherInts, SpecificIndex, NumberOfChoice)
	then
		NewDI = int.plus(DownIndex, 1),
		NewRI = int.plus(RightIndex, 3),
		Rest = no_of_trees(NewDI, NewRI, ListOfInts),
		( if
	  		NumberOfChoice = 46
	      	then
			N = Rest
		else
			N = int.plus(Rest, 1)
		)
	else
		N = 0
	).

main(!IO) :-
	io.open_input("input", ResStream, !IO),
	( if
		ResStream = io.ok(Stream)
	then
		process_stream(Stream, ListOfInts, !IO),
		Result = no_of_trees(0, 0, ListOfInts),
		io.print(Result, !IO)
	else
		io.write_string("Issue with input stream.", !IO)
	),
	io.nl(!IO).
