/* Advent of Code: Day 3, Part 2
* Author: Matthew Bardsley
*/
:- module part2.
:- interface.
:- import_module io.
:- pred main(io.io::di, io.io::uo) is det.
:- implementation.
:- import_module list, string, char, int.

:- type slope
	--->	slope(
			down :: int,
		       right :: int,
			list_of_ints :: list(list(int))
		     ).

:- pred process_stream(text_input_stream::in, list(list(int))::out, io::di, io::uo) is det.
process_stream(Stream, Xs, !IO) :-
	io.read_line_as_string(Stream, Result, !IO),
	( if 
		Result = io.ok(String),
		ListOfChars = to_char_list(chomp(String)),
		X = map(to_int, ListOfChars)
	then
		process_stream(Stream, Rest, !IO),
		Xs = [X | Rest]
	else
		Xs = []
	).

:- func no_of_trees(int, int, list(list(int)), int, int) = int.
no_of_trees(DownIndex, RightIndex, ListOfInts, DownSlope, RightSlope) = N :-
	( if
	  	index0(ListOfInts, DownIndex, FurtherInts),
		SpecificIndex = RightIndex mod length(FurtherInts),
		index0(FurtherInts, SpecificIndex, NumberOfChoice)
	then
		NewDI = DownIndex + DownSlope,
		NewRI = RightIndex + RightSlope,
		Rest = no_of_trees(NewDI, NewRI, ListOfInts, DownSlope, RightSlope),
		( if
	  		NumberOfChoice = 46
	      	then
			N = Rest
		else
			N = Rest + 1
		)
	else
		N = 0
	).

:- func no_of_trees_for_slope(slope, int) = int.
no_of_trees_for_slope(slope(Down, Right, ListOfInts), N) = no_of_trees(0, 0, ListOfInts, Down, Right) * N.

main(!IO) :-
	io.open_input("input", ResStream, !IO),
	( if
		ResStream = io.ok(Stream)
	then
		process_stream(Stream, ListOfInts, !IO),
		SlopesToCheck = [slope(1, 1, ListOfInts), slope(1, 3, ListOfInts), slope(1, 5, ListOfInts), slope(1, 7, ListOfInts), slope(2, 1, ListOfInts)],
		Result = foldl(no_of_trees_for_slope, SlopesToCheck, 1),
		io.print(Result, !IO)
	else
		io.write_string("Issue with input stream.", !IO)
	),
	io.nl(!IO).
