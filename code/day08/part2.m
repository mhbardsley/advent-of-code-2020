/* Advent of Code - Day 8, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, string, list, int, solutions.

:- pred changed_programs(list(string)::in, list(string)::out) is nondet.
changed_programs(Instructions, Result) :-
	member_index0(Inst1, Instructions, Ind1),
	NewInd1 = Ind1 + 1,
	member_index0(Inst2, Instructions, Ind2),
	NewInd2 = Ind2 + 1,
	remove_prefix("nop ", Inst1, Op1),
	remove_prefix("jmp ", Inst2, Op2),
	append("jmp ", Op1, NewInst1),
	append("nop ", Op2, NewInst2),
	(
		replace_nth(Instructions, NewInd1, NewInst1, Result)
	;
		replace_nth(Instructions, NewInd2, NewInst2, Result)
	).

:- pred execute(int::in, int::in, list(int)::in, list(string)::in, int::out) is semidet.
execute(Current, AccVal, PreviousIndices, Instructions, Result) :-
	not member(Current, PreviousIndices),
	length(Instructions, InstLength),
	( if
		InstLength =< Current
	then
		Result = AccVal
	else
		cons(Current, PreviousIndices, NewIndices),
		index0(Instructions, Current, Instruction),
		execute(New, NewAcc, NewIndices, Instructions, Result),
		( if
			prefix(Instruction, "nop")
		then
			New = Current + 1,
			NewAcc = AccVal
		else if
			remove_prefix("acc ", Instruction, Operand)
		then
			to_int(Operand, OperandInt),
			NewAcc = AccVal + OperandInt,
			New = Current + 1
		else
			remove_prefix("jmp ", Instruction, Operand),
			to_int(Operand, OperandInt),
			New = Current + OperandInt,
			NewAcc = AccVal
		)
	).

main(!IO) :-
	process_stream(!IO, Instructions),
	%execute(Instructions, 0, 0, [], Result),
	solutions(changed_programs(Instructions), Programs),
	filter_map(execute(0, 0, []), Programs, Result),
	io.print(Result, !IO),
	io.nl(!IO).
