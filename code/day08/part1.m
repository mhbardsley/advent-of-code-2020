/* Advent of Code - Day 8, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, string, list, int.

:- pred execute(list(string)::in, int::in, int::in, list(int)::in, int::out) is det.
execute(Instructions, CurrentInstructionIndex, AccumulatorVal, PreviousInstructionIndices, FirstDuplicate) :-
	( if
	  	member(CurrentInstructionIndex, PreviousInstructionIndices)
	then
		FirstDuplicate = AccumulatorVal
	else
		det_index0(Instructions, CurrentInstructionIndex, Instruction),
		cons(CurrentInstructionIndex, PreviousInstructionIndices, NewPreviousInstructionIndices),
		( if
	  		prefix(Instruction, "nop ")
		then
			NewInstructionIndex = CurrentInstructionIndex + 1,
			execute(Instructions, NewInstructionIndex, AccumulatorVal, NewPreviousInstructionIndices, FirstDuplicate)
		else if
			prefix(Instruction, "acc ")
		then
			det_remove_prefix("acc ", Instruction, AccOperandStr),
			AccOperand = det_to_int(AccOperandStr),
			NewAccVal = AccumulatorVal + AccOperand,
			NewInstructionIndex = CurrentInstructionIndex + 1,
			execute(Instructions, NewInstructionIndex, NewAccVal, NewPreviousInstructionIndices, FirstDuplicate)
		else
			det_remove_prefix("jmp ", Instruction, JmpOperandStr),
			JmpOperand = det_to_int(JmpOperandStr),
			NewInstructionIndex = CurrentInstructionIndex + JmpOperand,
			execute(Instructions, NewInstructionIndex, AccumulatorVal, NewPreviousInstructionIndices, FirstDuplicate)
		)
	).

main(!IO) :-
	process_stream(!IO, Instructions),
	execute(Instructions, 0, 0, [], Result),
	io.print(Result, !IO),
	io.nl(!IO).
