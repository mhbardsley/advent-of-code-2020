/* Advent of Code 2020 - Day 20, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, char, require, solutions, int,
	math, float.

:- type tile
	--->	tile(
			tile_no :: int,
			left :: string,
			top :: string,
			right :: string,
			bottom :: string,
			body :: list(string)
		).

:- type arrangement
	--->	arrangement(
			x :: int,
			y :: int,
			placed_tile :: tile
		).

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	tile_strings(StringRep, TileStrings),
	to_tiles(TileStrings, TileList),
	get_arrangement(TileList, Arrangements),
	get_bodies(Arrangements, Order),
	remove_borders(Order, Pruned),
	arrange(Pruned, ArrangedTile),
	get_hashes(ArrangedTile, Result).

:- pred tile_strings(list(string)::in, list(list(string))::out) is det.
tile_strings(StringRep, TileStrings) :-
	foldr2(get_tile, StringRep, [], FinalTile, [], Temp),
	TileStrings = [FinalTile | Temp].

:- pred get_tile(string::in, list(string)::in, list(string)::out,
		list(list(string))::in, list(list(string))::out) is det.
get_tile(Line, Current, NewCur, ResultIn, ResultOut) :-
	( if
		Line = ""
	then
		NewCur = [],
		ResultOut = [Current | ResultIn]
	else
		NewCur = [Line | Current],
		ResultOut = ResultIn
	).

:- pred to_tiles(list(list(string))::in, list(tile)::out) is det.
to_tiles(TileStrings, Tiles) :-
	map(to_tile, TileStrings, Tiles).

:- pred to_tile(list(string)::in, tile::out) is det.
to_tile(TileString, Tile) :-
	( if
		TileString = [StringNo | Mapping]
	then
		Tile = tile(Number, Left, Top, Right, Bot, Mapping),
		get_tile_no(StringNo, Number),
		get_vals(Mapping, Left, Top, Right, Bot)
	else
		error("to_wall_no")
	).

:- pred get_tile_no(string::in, int::out) is det.
get_tile_no(Line, Result) :-
	( if
		Words = words(Line),
		Words = ["Tile", NumberWithColon],
		remove_suffix(NumberWithColon, ":", Num),
		to_int(Num, Res)
	then
		Result = Res
	else
		error("get_tile_no")
	).

:- pred get_vals(list(string)::in, string::out, string::out,
	string::out, string::out) is det.
get_vals(TileVals, Left, Top, Right, Bot) :-
	get_left(TileVals, Left),
	get_top(TileVals, Top),
	get_right(TileVals, Right),
	get_bot(TileVals, Bot).

:- pred get_left(list(string)::in, string::out) is det.
get_left(TileVals, Result) :-
	foldl(get_first_char, TileVals, [], CharList),
	from_char_list(CharList, Result).

:- pred get_first_char(string::in, list(char)::in, list(char)::out) is det.
get_first_char(String, CharList, Result) :-
	( if
		first_char(String, FirstChar, _)
	then
		Result = [FirstChar | CharList]
	else
		error("get_first_char")
	).

:- pred get_top(list(string)::in, string::out) is det.
get_top(TileVals, Result) :-
	( if
		X = head(TileVals)
	then
		Result = X
	else
		error("get_top")
	).

:- pred get_right(list(string)::in, string::out) is det.
get_right(TileVals, Result) :-
	foldr(get_last_char, TileVals, [], CharList),
	from_char_list(CharList, Result).

:- pred get_last_char(string::in, list(char)::in, list(char)::out) is det.
get_last_char(String, CharList, Result) :-
	to_char_list(String, LineAsChars),
	( if
		last(LineAsChars, Char)
	then
		Result = [Char | CharList]
	else
		error("get_last_char")
	).

:- pred get_bot(list(string)::in, string::out) is det.
get_bot(TileVals, Result) :-
	( if
		last(TileVals, X)
	then
		rev(X, Result)
	else
		error("get_bot")
	).

:- pred get_arrangement(list(tile)::in, list(arrangement)::out) is det.
get_arrangement(Tiles, Arrangement) :-
	length(Tiles, TileLength),
	MaxWidth = round_to_int(sqrt(float(TileLength))),
	solutions(find_arrangement(MaxWidth, 0, Tiles, []), Arrangements),
	( if
		Arrangements = [X | _]
	then
		Arrangement = X
	else
		error("get_arrangement")
	).

:- pred get_rotations_flips(tile::in, list(tile)::out) is det.
get_rotations_flips(Tile, Perms) :-
	solutions(get_rotation_flip(0, Tile), Perms).

:- pred get_rotation_flip(int::in, tile::in, tile::out) is multi.
get_rotation_flip(Index, TileIn, TileOut) :-
	TileOut = TileIn
	;
	Index < 3,
	(rotate_90(TileIn, NextTile)
	;
	flip_horizontal(TileIn, NextTile)
	;
	flip_vertical(TileIn, NextTile)
	),
	get_rotation_flip(Index + 1, NextTile, TileOut).

:- pred rotate_90(tile::in, tile::out) is det.
rotate_90(TileIn, TileOut) :-
	TileIn = tile(Id, L, T, R, B, Body),
	TileOut = tile(Id, B, L, T, R, Body).


:- pred flip_horizontal(tile::in, tile::out) is det.
flip_horizontal(TileIn, TileOut) :-
	TileIn = tile(Id, L, T, R, B, Body),
	rev(L, RevL),
	rev(T, RevT),
	rev(R, RevR),
	rev(B, RevB),
	TileOut = tile(Id, RevR, RevT, RevL, RevB, Body).

:- pred flip_vertical(tile::in, tile::out) is det.
flip_vertical(TileIn, TileOut) :-
	TileIn = tile(Id, L, T, R, B, Body),
	rev(L, RevL),
	rev(T, RevT),
	rev(R, RevR),
	rev(B, RevB),
	TileOut = tile(Id, RevL, RevB, RevR, RevT, Body).
:- pred rev(string::in, string::out) is det.
rev(StringIn, StringOut) :-
	to_char_list(StringIn, CharList),
	reverse(CharList, ReverseCharList),
	from_char_list(ReverseCharList, StringOut).

:- pred find_arrangement(int::in, int::in, list(tile)::in, 
	list(arrangement)::in, list(arrangement)::out) is nondet.
find_arrangement(MaxWidth, Index, Tiles, InArr, OutArr) :-
	( if
		Tiles = []
	then
		OutArr = InArr
	else
		PotentialAbove = Index - MaxWidth,
		HasLeft = Index mod MaxWidth,
		( if
			PotentialAbove >= 0,
			HasLeft \= 0
		then
			get_coords(MaxWidth, PotentialAbove, AboveX, AboveY),
			get_coords(MaxWidth, Index - 1, LeftX, LeftY),
			member(arrangement(AboveX, AboveY, 
				tile(_, _, _, _, B, _)), InArr),
			member(arrangement(LeftX, LeftY, 
				tile(_, _, _, R, _, _)), InArr),
			some [NextTile]
			(
				member(NextTile, Tiles),
				get_rotations_flips(NextTile, RotationsFlips),
				member(RF, RotationsFlips),
				RF = tile(_, L, T, _, _, _),
				rev(R, RevR),
				rev(B, RevB),
				RevR = L,
				RevB = T,
				delete(Tiles, NextTile, Next),
				get_coords(MaxWidth, Index, X, Y),
				Arrangement = arrangement(X, Y, RF),
				NewArr = [Arrangement | InArr],
				find_arrangement(MaxWidth, Index + 1, Next,
				NewArr, OutArr)
			)
		else if
			PotentialAbove >= 0
		then
			get_coords(MaxWidth, PotentialAbove, AboveX, AboveY),
			member(arrangement(AboveX, AboveY, 
				tile(_, _, _, _, B, _)), InArr),
			some [NextTile]
			(
				member(NextTile, Tiles),
				get_rotations_flips(NextTile, RotationsFlips),
				member(RF, RotationsFlips),
				RF = tile(_, _, T, _, _, _),
				rev(B, RevB),
				RevB = T,
				delete(Tiles, NextTile, Next),
				get_coords(MaxWidth, Index, X, Y),
				Arrangement = arrangement(X, Y, RF),
				NewArr = [Arrangement | InArr],
				find_arrangement(MaxWidth, Index + 1, Next,
				NewArr, OutArr)
			)
		else if
			HasLeft \= 0
		then
			get_coords(MaxWidth, Index - 1, LeftX, LeftY),
			member(arrangement(LeftX, LeftY, 
				tile(_, _, _, R, _, _)), InArr),
			some [NextTile]
			(
				member(NextTile, Tiles),
				get_rotations_flips(NextTile, RotationsFlips),
				member(RF, RotationsFlips),
				RF = tile(_, L, _, _, _, _),
				rev(R, RevR),
				RevR = L,
				delete(Tiles, NextTile, Next),
				get_coords(MaxWidth, Index, X, Y),
				Arrangement = arrangement(X, Y, RF),
				NewArr = [Arrangement | InArr],
				find_arrangement(MaxWidth, Index + 1, Next,
				NewArr, OutArr)
			)
		else
			some [NextTile]
			(
				member(NextTile, Tiles),
				get_rotations_flips(NextTile, RotationsFlips),
				member(RF, RotationsFlips),
				delete(Tiles, NextTile, Next),
				get_coords(MaxWidth, Index, X, Y),
				Arrangement = arrangement(X, Y, RF),
				NewArr = [Arrangement | InArr],
				find_arrangement(MaxWidth, Index + 1, Next,
				NewArr, OutArr)
			)
		)
	
	).

:- pred get_coords(int::in, int::in, int::out, int::out) is det.
get_coords(MaxWidth, Index, X, Y) :-
	X = Index mod MaxWidth,
	Y = Index // MaxWidth.

:- pred rotate_body(list(string)::in, list(string)::out) is det.
rotate_body(Body, NewBody) :-
	with_rotated(Body, NewBody).

:- pred with_rotated(list(string)::in, list(string)::out) is det.
with_rotated(Body, NewBody) :-
	( if
		all [X]
		(
			member(X, Body) => X = ""
		)
	then
		NewBody = []
	else
		foldl(add_head, Body, "", Heads),
		foldr(get_rest, Body, [], Rest),
		with_rotated(Rest, RestOfBody),
		NewBody = [Heads | RestOfBody]
	).

:- pred add_head(string::in, string::in, string::out) is det.
add_head(Line, InStr, OutStr) :-
	to_char_list(Line, CharList),
	to_char_list(InStr, InCharList),
	( if
		X = head(CharList)
	then
		OutCharList = [X | InCharList],
		from_char_list(OutCharList, OutStr)
	else
		error("add_head")
	).
		
:- pred get_rest(string::in, list(string)::in, list(string)::out) is det.
get_rest(Line, InRest, OutRest) :-
	to_char_list(Line, CharList),
	( if
		Xs = tail(CharList)
	then
		from_char_list(Xs, StrXs),
		OutRest = [StrXs | InRest]
	else
		error("get_rest")
	).

:- pred flip_horizontal_body(list(string)::in, list(string)::out) is det.
flip_horizontal_body(Body, NewBody) :-
	map(rev, Body, NewBody).

:- pred flip_vertical_body(list(string)::in, list(string)::out) is det.
flip_vertical_body(Body, NewBody) :-
	rotate_body(Body, T1),
	flip_horizontal_body(T1, T2),
	rotate_body(T2, T3),
	rotate_body(T3, T4),
	rotate_body(T4, NewBody).

:- pred change_body(arrangement::in, arrangement::out) is det.
change_body(ArrIn, ArrOut) :-
	ArrIn = arrangement(X, Y, tile(Id, L, T, R, B, BodyIn)),
	ArrOut = arrangement(X, Y, tile(Id, L, T, R, B, BodyOut)),
	solutions(possible_body(L, T, R, B, 0, BodyIn), Bodies),
	( if
		Head = head(Bodies)
	then
		BodyOut = Head
	else
		error("get_body")
	).

:- pred possible_body(string::in, string::in, string::in, string::in,
	int::in, list(string)::in, list(string)::out) is nondet.
possible_body(L, T, R, B, Index, Body, NextBody) :-
	(
		NextBody = Body,
		get_left(Body, Left),
		get_top(Body, Top),
		get_right(Body, Right),
		get_bot(Body, Bottom),
		L = Left,
		T = Top,
		R = Right,
		B = Bottom
	)
	;
	Index < 3,
	(rotate_body(Body, Temp)
	;
	flip_horizontal_body(Body, Temp)
	;
	flip_vertical_body(Body, Temp)
	),
	possible_body(L, T, R, B, Index + 1, Temp, NextBody).

:- pred get_bodies(list(arrangement)::in, list(arrangement)::out) is det.
get_bodies(Arrangements, Result) :-
	map(change_body, Arrangements, Result).

:- pred arrange(list(arrangement)::in, list(string)::out) is det.
arrange(Arrangements, Result) :-
	length(Arrangements, Length),
	MaxY = round_to_int(sqrt(float(Length))),
	ListForFold = 0 .. MaxY - 1,
	foldr(create_grid(Arrangements), ListForFold, [], ListOfArrs),
	map(to_lines, ListOfArrs, Lines),
	condense(Lines, Result).

:- pred create_grid(list(arrangement)::in, int::in, 
	list(list(arrangement))::in, list(list(arrangement))::out) is det.
create_grid(Arrangements, Y, Current, Result) :-
	length(Arrangements, Length),
	MaxX = round_to_int(sqrt(float(Length))),
	ListForFold = 0 .. MaxX - 1,
	foldr(create_order(Arrangements, Y), ListForFold, [], Row),
	Result = [Row | Current].

:- pred create_order(list(arrangement)::in, int::in, int::in,
	list(arrangement)::in, list(arrangement)::out) is det.
create_order(Arrangements, Y, X, Current, Result) :-
	solutions(get_member(Arrangements, Y, X), Sols),
	( if
		Next = head(Sols)
	then
		Result = [Next | Current]
	else
		error("create_order")
	).

:- pred get_member(list(arrangement)::in, int::in, int::in,
	arrangement::out) is nondet.
get_member(Arrangements, Y, X, Result) :-
	member(Result, Arrangements),
	Result = arrangement(X, Y, _).

:- pred to_lines(list(arrangement)::in, list(string)::out) is det.
to_lines(Arrangements, Result) :-
	map(get_string, Arrangements, StringLists),
	concat_segment(StringLists, Result).

:- pred get_string(arrangement::in, list(string)::out) is det.
get_string(Arrangement, Result) :-
	Arrangement = arrangement(_, _, tile(_, _, _, _, _, Result)).

:- pred concat_segment(list(list(string))::in, list(string)::out) is det.
concat_segment(Bodies, Result) :-
	( if
		all [X]
		(
			member(X, Bodies) => X = []
		)
	then
		Result = []
	else
		ListOfStrings = map(det_head, Bodies),
		Rest = map(det_tail, Bodies),
		append_list(ListOfStrings, Line),
		concat_segment(Rest, OtherLines),
		Result = [Line | OtherLines]
	).

:- pred remove_borders(list(arrangement)::in, list(arrangement)::out) is det.
remove_borders(TileIn, TileOut) :-
	map(remove_a_border, TileIn, TileOut).

:- pred remove_a_border(arrangement::in, arrangement::out) is det.
remove_a_border(ArrIn, ArrOut) :-
	ArrIn = arrangement(X, Y, tile(Id, L, T, R, B, BodyIn)),
	ArrOut = arrangement(X, Y, tile(Id, L, T, R, B, BodyOut)),
	remove_left(BodyIn, T1),
	remove_top(T1, T2),
	remove_right(T2, T3),
	remove_bot(T3, BodyOut).

:- pred remove_left(list(string)::in, list(string)::out) is det.
remove_left(TileIn, TileOut) :-
	map(retain_right, TileIn, TileOut).

:- pred retain_right(string::in, string::out) is det.
retain_right(StringIn, StringOut) :-
	to_char_list(StringIn, CharList),
	( if
		X = tail(CharList)
	then
		from_char_list(X, StringOut)
	else
		error("retain_right")
	).

:- pred remove_top(list(string)::in, list(string)::out) is det.
remove_top(TileIn, TileOut) :-
	( if
		X = tail(TileIn)
	then
		TileOut = X
	else
		error("remove_top")
	).

:- pred remove_right(list(string)::in, list(string)::out) is det.
remove_right(TileIn, TileOut) :-
	map(retain_left, TileIn, TileOut).

:- pred retain_left(string::in, string::out) is det.
retain_left(StringIn, StringOut) :-
	to_char_list(StringIn, CharList),
	( if
		split_last(CharList, X, _)
	then
		from_char_list(X, StringOut)
	else
		error("retain_left")
	).

:- pred remove_bot(list(string)::in, list(string)::out) is det.
remove_bot(TileIn, TileOut) :-
	( if
		split_last(TileIn, X, _)
	then
		TileOut = X
	else
		error("remove_bot")
	).

:- pred get_hashes(list(string)::in, int::out) is det.
get_hashes(BigTile, Result) :-
	get_bigtile_changes(BigTile, Changes),
	filter(has_sea_monster, Changes, WithSeaMonster),
	( if
		WithSeaMonster = [X]
	then
		insert_sea_monsters(X, FinalMap),
		count_hashes(FinalMap, Result)
	else
		error("get_hashes")
	).

:- pred get_bigtile_changes(list(string)::in, list(list(string))::out) is det.
get_bigtile_changes(Body, Bodies) :-
	solutions(get_bigtile_change(0, Body), Bodies).

:- pred get_bigtile_change(int::in, list(string)::in, 
	list(string)::out) is multi.
get_bigtile_change(Index, InBody, OutBody) :-
	InBody = OutBody
	;
	Index < 3,
	(rotate_body(InBody, Temp)
	;
	flip_horizontal_body(InBody, Temp)
	;
	flip_vertical_body(InBody, Temp)
	),
	get_bigtile_change(Index + 1, Temp, OutBody).
	
:- pred has_sea_monster(list(string)::in) is semidet.
has_sea_monster(Body) :-
	length(Body, NoOfLines),
	some [Line1Index, Line2Index, Line3Index]
	(
		PossibleLineIndices = 0 .. NoOfLines - 3,
		member(Line1Index, PossibleLineIndices),
		Line2Index = Line1Index + 1,
		Line3Index = Line2Index + 1,
		index0(Body, Line1Index, Line1),
		index0(Body, Line2Index, Line2),
		index0(Body, Line3Index, Line3),
		to_char_list(Line1, Line1Chars),
		to_char_list(Line2, Line2Chars),
		to_char_list(Line3, Line3Chars),
		member_indexes0('#', Line1Chars, Line1Indices),
		member_indexes0('#', Line2Chars, Line2Indices),
		member_indexes0('#', Line3Chars, Line3Indices),
		conforms(Line1Indices, Line2Indices, Line3Indices)
	).

:- pred conforms(list(int)::in, list(int)::in, list(int)::in) is semidet.
conforms(L1Indices, L2Indices, L3Indices) :-
	some [A, B, C, D, E, F, G, H, I, J, K, L, M, N, O]
	(
		B = A - 18,
		C = B + 5,
		D = C + 1,
		E = D + 5,
		F = E + 1,
		G = F + 5,
		H = G + 1,
		I = H + 1,
		J = I - 18,
		K = J + 3,
		L = K + 3,
		M = L + 3,
		N = M + 3,
		O = N + 3,
		member(A, L1Indices),
		member(B, L2Indices),
		member(C, L2Indices),
		member(D, L2Indices),
		member(E, L2Indices),
		member(F, L2Indices),
		member(G, L2Indices),
		member(H, L2Indices),
		member(I, L2Indices),
		member(J, L3Indices),
		member(K, L3Indices),
		member(L, L3Indices),
		member(M, L3Indices),
		member(N, L3Indices),
		member(O, L3Indices)
	).
		
:- pred insert_sea_monsters(list(string)::in, list(string)::out) is det.
insert_sea_monsters(BodyIn, BodyOut) :-
	( if
		solutions(change(BodyIn), PotentialNewBodies),
		X = head(PotentialNewBodies)
	then
		insert_sea_monsters(X, BodyOut)
	else
		BodyOut = BodyIn
	).

:- pred change(list(string)::in, list(string)::out) is nondet.
change(Body, BodyOut) :-
	length(Body, NoOfLines),
	some [Line1Index, Line2Index, Line3Index]
	(
		PossibleLineIndices = 0 .. NoOfLines - 3,
		member(Line1Index, PossibleLineIndices),
		Line2Index = Line1Index + 1,
		Line3Index = Line2Index + 1,
		index0(Body, Line1Index, Line1),
		index0(Body, Line2Index, Line2),
		index0(Body, Line3Index, Line3),
		to_char_list(Line1, Line1Chars),
		to_char_list(Line2, Line2Chars),
		to_char_list(Line3, Line3Chars),
		change_conforms(Line1Chars, Line2Chars, Line3Chars,
			NewL1Chars, NewL2Chars, NewL3Chars),
		from_char_list(NewL1Chars, NewL1),
		from_char_list(NewL2Chars, NewL2),
		from_char_list(NewL3Chars, NewL3),
		replace_nth(Body, Line1Index + 1, NewL1, Body2),
		replace_nth(Body2, Line2Index + 1, NewL2, Body3),
		replace_nth(Body3, Line3Index + 1, NewL3, BodyOut)
	).

:- pred change_conforms(list(char)::in, list(char)::in, list(char)::in,
			list(char)::out, list(char)::out, list(char)::out)
			is nondet.
change_conforms(L1Chars, L2Chars, L3Chars, NewL1Chars, NewL2Chars, NewL3Chars) :-
	member_indexes0('#', L1Chars, L1Indices),
	member_indexes0('#', L2Chars, L2Indices),
	member_indexes0('#', L3Chars, L3Indices),
	some [A, B, C, D, E, F, G, H, I, J, K, L, M, N, O]
	(
		B = A - 18,
		C = B + 5,
		D = C + 1,
		E = D + 5,
		F = E + 1,
		G = F + 5,
		H = G + 1,
		I = H + 1,
		J = I - 18,
		K = J + 3,
		L = K + 3,
		M = L + 3,
		N = M + 3,
		O = N + 3,
		member(A, L1Indices),
		member(B, L2Indices),
		member(C, L2Indices),
		member(D, L2Indices),
		member(E, L2Indices),
		member(F, L2Indices),
		member(G, L2Indices),
		member(H, L2Indices),
		member(I, L2Indices),
		member(J, L3Indices),
		member(K, L3Indices),
		member(L, L3Indices),
		member(M, L3Indices),
		member(N, L3Indices),
		member(O, L3Indices),
		replace_nth(L1Chars, A + 1, 'O', NewL1Chars),
		replace_nth(L2Chars, B + 1, 'O', L2Chars2),
		replace_nth(L2Chars2, C + 1, 'O', L2Chars3),
		replace_nth(L2Chars3, D + 1, 'O', L2Chars4),
		replace_nth(L2Chars4, E + 1, 'O', L2Chars5),
		replace_nth(L2Chars5, F + 1, 'O', L2Chars6),
		replace_nth(L2Chars6, G + 1, 'O', L2Chars7),
		replace_nth(L2Chars7, H + 1, 'O', L2Chars8),
		replace_nth(L2Chars8, I + 1, 'O', NewL2Chars),
		replace_nth(L3Chars, J + 1, 'O', L3Chars2),
		replace_nth(L3Chars2, K + 1, 'O', L3Chars3),
		replace_nth(L3Chars3, L + 1, 'O', L3Chars4),
		replace_nth(L3Chars4, M + 1, 'O', L3Chars5),
		replace_nth(L3Chars5, N + 1, 'O', L3Chars6),
		replace_nth(L3Chars6, O + 1, 'O', NewL3Chars)
	).


:- pred count_hashes(list(string)::in, int::out) is det.
count_hashes(BodyIn, Result) :-
	foldl(hash_count, BodyIn, 0, Result).

:- pred hash_count(string::in, int::in, int::out) is det.
hash_count(Line, AccVal, Result) :-
	to_char_list(Line, LineChars),
	filter(is_hash, LineChars, Hashes),
	length(Hashes, NoOfHashes),
	Result = AccVal + NoOfHashes.

:- pred is_hash(char::in) is semidet.
is_hash(Char) :-
	Char = '#'.
