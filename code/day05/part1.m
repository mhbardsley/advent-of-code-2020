/* Advent of Code: Day 5, Part 1
* Author: Matthew Bardsley
*/
:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, char, int.

:- pred char_mapping(char::in, char::out) is semidet.
char_mapping('B', '1').
char_mapping('R', '1').
char_mapping('F', '0').
char_mapping('L', '0').

:- pred to_binary(string::in, string::out) is det.
to_binary(String, Result) :-
	to_char_list(String, CharList),
	filter_map(char_mapping, CharList, OutCharList),
	from_char_list(OutCharList, Result).

:- pred to_slot(string::in, int::out) is det.
to_slot(String, Result) :-
	split(String, 7, A, B),
	Row = det_base_string_to_int(2, A),
	Column = det_base_string_to_int(2, B),
	Row * 8 = T1,
	T1 + Column = Result.

main(!IO) :-
	process_stream(!IO, MyList),
	map(to_binary, MyList, Binaries),
	map(to_slot, Binaries, Slots),
	sort(Slots, SortedSlots),
	det_last(SortedSlots, Result),
	io.print(Result, !IO),
	io.nl(!IO).
