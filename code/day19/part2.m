/* Advent of Code 2020 - Day 19, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, char, bool, list, string, int.

:- type cnf
	--->	cnf(
			index :: int,
			rules :: cnf_rule
		).

:- type cnf_rule
	--->	units(int, int)
	;	terminal(char).


:- type byk
	--->	byk(
			grammar :: list(cnf),
			test_string :: string,
			p_array :: list(list(list(bool)))
		).

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	to_cnf_grammar(StringRep, Grammar),
	to_strings(StringRep, Strings),
	run_tests(Grammar, Strings, Conformants),
	no_of_conformants(Conformants, Result).

:- pred to_cnf_grammar(list(string)::in, list(cnf)::out) is det.
to_cnf_grammar(StringRep, Grammar) :-
	( if
		StringRep = [String | Others]
	then
		to_cnf_grammar(Others, Rest),
		( if
			string_to_grammar(String, Cnfs)
		then
			Grammar = Cnfs ++ Rest
		else
			Grammar = Rest
		)
	else
		Grammar = []
	).

:- pred string_to_grammar(string::in, list(cnf)::out) is semidet.
string_to_grammar(Line, Cnf) :-
	LineWords = words(Line),
	Lhs = head(LineWords),
	Rhs = tail(LineWords),
	get_index(Lhs, Index),
	get_rules(Rhs, Rules),
	make_cnfs(Index, Rules, Cnf).

:- pred get_index(string::in, int::out) is semidet.
get_index(FirstWord, Index) :-
	remove_suffix(FirstWord, ":", NumStr),
	to_int(NumStr, Index).

:- pred get_rules(list(string)::in, list(cnf_rule)::out) is semidet.
get_rules(Strings, Rules) :-
	separate_rules(Strings, Separated),
	map(to_cnf, Separated, Rules).

:- pred separate_rules(list(string)::in, list(list(string))::out) is det.
separate_rules(Words, Separated) :-
	RhsFlat = join_list(" ", Words),
	SeparatedRules = split_at_char('|', RhsFlat),
	Separated = map(words, SeparatedRules).

:- pred to_cnf(list(string)::in, cnf_rule::out) is semidet.
to_cnf(RulePart, Result) :-
	RulePart = [NumS1, NumS2],
	to_int(NumS1, Int1),
	to_int(NumS2, Int2),
	Result = units(Int1, Int2)
	;
	RulePart = [String],
	to_char_list(String, CharList),
	CharList = ['"', Char, '"'],
	Result = terminal(Char).

:- pred make_cnfs(int::in, list(cnf_rule)::in, list(cnf)::out) is det.
make_cnfs(Index, Rules, Result) :-
	map(make_cnf(Index), Rules, Result).

:- pred make_cnf(int::in, cnf_rule::in, cnf::out) is det.
make_cnf(Index, Rule, Result) :-
	Result = cnf(Index, Rule).

:- pred to_strings(list(string)::in, list(string)::out) is det.
to_strings(StringRep, Result) :-
	filter(is_test_string, StringRep, Result).

:- pred is_test_string(string::in) is semidet.
is_test_string(String) :-
	length(String, Length),
	Length > 0,
	to_char_list(String, CharList),
	not member(':', CharList).

:- pred run_tests(list(cnf)::in, list(string)::in, list(string)::out) is det.
run_tests(Grammar, Lines, Result) :-
	map(to_byk(Grammar), Lines, Byks),
	filter(run_test, Byks, Res),
	map(to_str, Res, Result).

:- pred to_byk(list(cnf)::in, string::in, byk::out) is det.
to_byk(Grammar, String, Byk) :-
	get_p_array(Grammar, String, PArray),
	Byk = byk(Grammar, String, PArray).

:- pred get_p_array(list(cnf)::in, string::in, 
	list(list(list(bool)))::out) is det.
get_p_array(Grammar, Words, Result) :-
	length(Words, N),
	length(Grammar, R),
	initialise_p(0, N, R, Result).

:- pred initialise_p(int::in, int::in, int::in, 
	list(list(list(bool)))::out) is det.
initialise_p(X, N, R, Result) :-
	( if
		X < N
	then
		initialise_2d(0, N, R, Val),
		initialise_p(X + 1, N, R, SubRest),
		Result = [Val | SubRest]
	else
		Result = []
	).

:- pred initialise_2d(int::in, int::in, int::in, list(list(bool))::out) is det.
initialise_2d(Y, N, R, Result) :-
	( if
		Y < N
	then
		initialise_1d(0, R, Val),
		initialise_2d(Y + 1, N, R, SubRest),
		Result = [Val | SubRest]
	else
		Result = []
	).

:- pred initialise_1d(int::in, int::in, list(bool)::out) is det.
initialise_1d(Z, R, Result) :-
	( if
		Z < R
	then
		initialise_1d(Z + 1, R, SubRest),
		Result = [no | SubRest]
	else
		Result = []
	).

:- pred run_test(byk::in) is semidet.
run_test(Byk) :-
	replace_units(Byk, UnitsReplaced),
	main_run(UnitsReplaced, BykOut),
	is_member(BykOut).

:- pred replace_units(byk::in, byk::out) is det.
replace_units(InByk, OutByk) :-
	InByk = byk(_, String, _),
	length(String, Length),
	set_units(0, Length, InByk, OutByk).

:- pred set_units(int::in, int::in, byk::in, byk::out) is det.
set_units(S, Limit, InByk, OutByk) :-
	( if
		S < Limit
	then
		set_units(S + 1, Limit, NewByk, OutByk),
		replace_rules(S, InByk, NewByk)
	else
		OutByk = InByk
	).

:- pred replace_rules(int::in, byk::in, byk::out) is det.
replace_rules(S, In, Out) :-
	In = byk(Grammar, String, Bools),
	det_index(String, S, Char),
	get_rules(1, Grammar, Char, Vs),
	replace_svs(S + 1, Vs, Bools, NewBools),
	Out = byk(Grammar, String, NewBools).

:- pred get_rules(int::in, list(cnf)::in, char::in, list(int)::out) is det.
get_rules(Index, Rules, Char, Vs) :-
	( if
		Rules = [Rule | Others]
	then
		get_rules(Index + 1, Others, Char, OtherVs),
		( if
			Rule = cnf(_, terminal(Char))
		then
			Vs = [Index | OtherVs]
		else
			Vs = OtherVs
		)
	else
		Vs = []
	).

:- pred replace_svs(int::in, list(int)::in, list(list(list(bool)))::in, 
	list(list(list(bool)))::out) is det.
replace_svs(S, Vs, Bools, NewBools) :-
	foldl(replace_sv(S), Vs, Bools, NewBools).

:- pred replace_sv(int::in, int::in, list(list(list(bool)))::in, 
	list(list(list(bool)))::out) is det.
replace_sv(S, V, Bools, NewBools) :-
	det_index1(Bools, 1, YandZ),
	det_index1(YandZ, S, Zs),
	det_replace_nth(Zs, V, yes, NewZs),
	det_replace_nth(YandZ, S, NewZs, NewYandZ),
	det_replace_nth(Bools, 1, NewYandZ, NewBools).

:- pred main_run(byk::in, byk::out) is det.
main_run(In, Out) :-
	In = byk(_, String, _),
	length(String, N),
	Indices = 2 .. N,
	foldl(with_length, Indices, In, Out).

:- pred with_length(int::in, byk::in, byk::out) is det.
with_length(L, In, Out) :-
	In = byk(_, String, _),
	length(String, N),
	Indices = 1 .. N - L + 1,
	foldl(with_start(L), Indices, In, Out).

:- pred with_start(int::in, int::in, byk::in, byk::out) is det.
with_start(L, S, In, Out) :-
	Indices = 1 .. L - 1,
	foldl(check(L, S), Indices, In, Out).

:- pred check(int::in, int::in, int::in, byk::in, byk::out) is det.
check(L, S, P, In, Out) :-
	In = byk(Grammar, String, Bools),
	foldl(change(L, S, P, Grammar), Grammar, Bools, NewBools),
	Out = byk(Grammar, String, NewBools).

:- pred change(int::in, int::in, int::in, list(cnf)::in,
	cnf::in, list(list(list(bool)))::in, list(list(list(bool)))::out) 
	is det.
change(L, S, P, Rules, Rule, In, Out) :-
	Rule = cnf(_, terminal(_)),
	Out = In
	;
	Rule = cnf(_, units(U1, U2)),
	A = det_index1_of_first_occurrence(Rules, Rule),
	get_indices(U1, Rules, Bs),
	get_indices(U2, Rules, Cs),
	foldl(step(L, S, P, A, Cs), Bs, In, Out).

:- pred get_indices(int::in, list(cnf)::in, list(int)::out) is det.
get_indices(RuleNo, Rules, PossibleRules) :-
	filter_map(is_rule(RuleNo, Rules), Rules, PossibleRules).

:- pred is_rule(int::in, list(cnf)::in, cnf::in, int::out) is semidet.
is_rule(RuleNo, Rules, Rule, Index) :-
	Rule = cnf(RuleNo, _),
	Index = det_index1_of_first_occurrence(Rules, Rule).

:- pred step(int::in, int::in, int::in, int::in, list(int)::in,
	int::in, list(list(list(bool)))::in, list(list(list(bool)))::out)
	is det.
step(L, S, P, A, Cs, B, In, Out) :-
	foldl(final(L, S, P, A, B), Cs, In, Out).

:- pred final(int::in, int::in, int::in, int::in, int::in, int::in,
	list(list(list(bool)))::in, list(list(list(bool)))::out) is det.
final(L, S, P, A, B, C, In, Out) :-
	det_index1(In, P, T1),
	det_index1(T1, S, T2),
	det_index1(T2, B, Test1),
	det_index1(In, L - P, T3),
	det_index1(T3, S + P, T4),
	det_index1(T4, C, Test2),
	det_index1(In, L, T5),
	det_index1(T5, S, T6),
	( if
		Test1 = yes,
		Test2 = yes
	then
		det_replace_nth(T6, A, yes, T7),
		det_replace_nth(T5, S, T7, T8),
		det_replace_nth(In, L, T8, Out)
	else
		Out = In
	).

:- pred is_member(byk::in) is semidet.
is_member(Byk) :-
	Byk = byk(_, String, Bools),
	length(String, Length),
	det_index0(Bools, Length - 1, T1),
	det_index0(T1, 0, T2),
	det_index0(T2, 0, Test),
	Test = yes.

:- pred to_str(byk::in, string::out) is det.
to_str(Byk, String) :-
	Byk = byk(_, String, _).

:- pred no_of_conformants(list(string)::in, int::out) is det.
no_of_conformants(Strings, Result) :-
	length(Strings, Result).
