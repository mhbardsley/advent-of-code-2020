/* Advent of Code: Day 4, Part 2
* Author: Matthew Bardsley
*/
:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, char.

:- func normalise_current(list(string), string) = list(list(string)).
normalise_current(InitialList, Previous) = N :-
	( if
	  	Item = head(InitialList),
		Tail = tail(InitialList)
	then
		( if
		  	is_empty(Item)
		then
			Rest = normalise_current(Tail, ""),
			N = [words(rstrip(Previous)) | Rest]
		else
			NewStr = append(Item, " "),
			AddedToString = append(Previous, NewStr),
			N = normalise_current(Tail, AddedToString)
		)
	else
		N = [words(rstrip(Previous))]
	).

:- pred conforms(list(string)::in) is semidet.
conforms(ListOfAttributes) :-
	(
		length(ListOfAttributes) = 8
	;
		(
	 		length(ListOfAttributes) = 7,
	 		not (
				member(S, ListOfAttributes),
				prefix(S, "cid")
			)
		)
	),
	(
		member(Byr, ListOfAttributes),
		append("byr:", ByrVal, Byr),
		to_int(ByrVal, IntByr),
		member(IntByr, 1920 .. 2002)
	),
	(
	 	member(Iyr, ListOfAttributes),
		append("iyr:", IyrVal, Iyr),
		to_int(IyrVal, IntIyr),
		member(IntIyr, 2010 .. 2020)
	),
	(
	 	member(Eyr, ListOfAttributes),
		append("eyr:", EyrVal, Eyr),
		to_int(EyrVal, IntEyr),
		member(IntEyr, 2020 .. 2030)
	),
	(
	 	member(Hgt, ListOfAttributes),
		append("hgt:", HgtVal, Hgt),
		(
			(
		 		append(CmTest, "cm", HgtVal),
				to_int(CmTest, IntCm),
				member(IntCm, 150 .. 193)
			)
		;
			(
			 	append(InchTest, "in", HgtVal),
				to_int(InchTest, IntInch),
				member(IntInch, 59 .. 76)
			)
		)
	),
	(
	 	member(Hcl, ListOfAttributes),
		append("hcl:", HclVal, Hcl),
		append("#", HexCode, HclVal),
		length(HexCode, 6),
		to_char_list(HexCode, HexCodeChars),
		all [X]
		(
		 	member(X, HexCodeChars),
			is_hex_digit(X)
		)
	),
	(
	 	member(Ecl, ListOfAttributes),
		append("ecl:", EclVal, Ecl),
		member(EclVal, ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"])
	),
	(
	 	member(Pid, ListOfAttributes),
		append("pid:", PidVal, Pid),
		length(PidVal, 9),
		to_int(PidVal, _)
	).
	
main(!IO) :-
	process_stream(!IO, MyList),
	NewList = normalise_current(MyList, ""),
	filter(conforms, NewList, ThoseThatConform),
	length(ThoseThatConform, Result),
	io.print(Result, !IO),
	io.nl(!IO).
