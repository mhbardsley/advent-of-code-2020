/* Advent of Code: Day 4, Part 1
* Author: Matthew Bardsley
*/
:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string.

:- func normalise_current(list(string), string) = list(string).
normalise_current(InitialList, Previous) = N :-
	( if
	  	Item = head(InitialList),
		Tail = tail(InitialList)
	then
		( if
		  	is_empty(Item)
		then
			Rest = normalise_current(Tail, ""),
			N = [Previous | Rest]
		else
			NewStr = append(" ", Item),
			AddedToString = append(Previous, NewStr),
			N = normalise_current(Tail, AddedToString)
		)
	else
		N = [Previous]
	).

:- pred conforms(string::in) is semidet.
conforms(String) :-
	sub_string_search(String, " byr:", _),
	sub_string_search(String, " iyr:", _),
	sub_string_search(String, " eyr:", _),
	sub_string_search(String, " hgt:", _),
	sub_string_search(String, " hcl:", _),
	sub_string_search(String, " ecl:", _),
	sub_string_search(String, " pid:", _).

:- pred not_conforms(string::in) is semidet.
not_conforms(String) :-
	not conforms(String).

main(!IO) :-
	process_stream(!IO, MyList),
	NewList = normalise_current(MyList, ""),
	filter(conforms, NewList, ThoseThatConform),
	length(ThoseThatConform, Result),
	io.print(Result, !IO),
	io.nl(!IO).
