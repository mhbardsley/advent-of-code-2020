/* Advent of Code - Day 14, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, map, int, list, char, string.

% predicate for converting value to binary
:- pred value_to_binary(int::in, list(char)::out) is det.
value_to_binary(Value,  Result) :-
	( if
		Value > 0
	then
		NextVal = Value // 2,
		Binary = Value mod 2,
		Result = NextResult ++ [BinaryChar],
		BinaryChar = det_int_to_binary_digit(Binary),
		value_to_binary(NextVal, NextResult)
	else
		Result = []
	).

% predicate for masking a binary list
:- pred apply_mask(list(char)::in, list(char)::in, list(char)::out) is det.
apply_mask(Mask, Value, Result) :-
	( if
		split_last(Value, ValueFront, ValueBit)
	then
		( if
			split_last(Mask, MaskFront, MaskBit)
		then
			apply_mask(MaskFront, ValueFront, RestOf),
			( if
				is_binary_digit(MaskBit)
			then
				ChangedBit = MaskBit
			else
				ChangedBit = ValueBit
			)
		else
			apply_mask(Mask, ValueFront, RestOf),
			ChangedBit = ValueBit
		),
		Result = RestOf ++ [ChangedBit]
	else
		Result = []
	).

% predicate for converting binary list to current value
:- pred binary_to_value(list(char)::in, int::in, int::out) is det.
binary_to_value(Binary, Index, Result) :-
	( if
		split_last(Binary, Rest, Char),
		binary_digit_to_int(Char, BinVal)
	then
		binary_to_value(Rest, Index + 1, SubResult),
		pow(2, Index, Val),
		Result = BinVal * Val + SubResult
	else
		Result = 0
	).

% predicate to deal with instructions
:- pred instruction_parser(list(string)::in, list(char)::in, map(string, int)::in, int::out) is det.
instruction_parser(Instructions, Mask, InMemory, Result) :-
	( if
		Instructions = [X | Xs],
		[Variable, _, Operand] = words(X)
	then
		instruction_parser(Xs, NewMask, NewMemory, Result),
		( if
			Variable = "mask"
		then
			NewMask = to_char_list(Operand),
			NewMemory = InMemory
		else
			Value = det_to_int(Operand),
			value_to_binary(Value, Binary),
			pad_binary(Binary, 36, Padded),
			apply_mask(Mask, Padded, Temp),
			binary_to_value(Temp, 0, NewValue),
			set(Variable, NewValue, InMemory, NewMemory),
			NewMask = Mask
		)
	else
		Result = foldl(memory_sum, InMemory, 0)
	).

:- pred pad_binary(list(char)::in, int::in, list(char)::out) is det.
pad_binary(Binary, ToPadTo, Result) :-
	from_char_list(Binary, BString),
	length(BString, Length),
	AmountToPad = ToPadTo - Length,
	get_zeros(AmountToPad, Zeros),
	append(Zeros, BString, Temp),
	to_char_list(Temp, Result).

:- pred get_zeros(int::in, string::out) is det.
get_zeros(Number, Result) :-
	( if
		Number >= 0
	then
		get_zeros(Number - 1, SubResult),
		append("0", SubResult, Result)
	else
		Result = ""
	).

:- func memory_sum(string, int, int) = int.
memory_sum(_, V1, V2) = V1 + V2.

main(!IO) :-
	process_stream(!IO, Temp),
	instruction_parser(Temp, [], map.init, Result),
	io.print(Result, !IO),
	io.nl(!IO).
