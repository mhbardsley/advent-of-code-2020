/* Advent of Code - Day 14, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, string, solutions, list, map, bool, char, int.

:- type instruction
	--->	mask(
			mask_val :: list(boolx)
		)
		;
		memory(
			index :: int,
			memory_val :: int
		).
	

:- type boolx
	--->	one; zero; x.

% convert an instruction string to type
:- pred instruction_parser(string::in, instruction::out) is nondet.
instruction_parser(String, Result) :-
	[Variable, _, OperandStr] = words(String),
	((
		Variable = "mask",
		to_boolx_list(OperandStr, OperandBoolxList),
		Result = mask(OperandBoolxList)
	)
	;
	(
		to_int(OperandStr, Operand),
		remove_prefix("mem[", Variable, Suffix),
		remove_suffix(Suffix, "]", IndexStr),
		to_int(IndexStr, Index),
		Result = memory(Index, Operand)
	)).

:- pred det_instruction_parser(string::in, instruction::out) is det.
det_instruction_parser(String, Result) :-
	solutions(instruction_parser(String), Temp),
	Result = det_head(Temp).

:- pred execute_instruction(instruction::in, list(boolx)::in, list(boolx)::out, map(int, int)::in, map(int, int)::out) is det.
execute_instruction(Instruction, InMask, OutMask, InMem, OutMem) :-
	(
		Instruction = mask(BoolxList),
		OutMask = BoolxList,
		OutMem = InMem
	)
	;
	(	Instruction = memory(Address, Value),
		mask_address(InMask, Address, AddressMasked),
		solutions(get_address(AddressMasked), Addresses),
		set_vals(Addresses, Value, InMem, OutMem),
		OutMask = InMask
	).

:- pred set_vals(list(int)::in, int::in, map(int, int)::in, map(int, int)::out) is det.
set_vals(Addresses, Value, InMem, OutMem) :-
	foldl(set_val(Value), Addresses, InMem, OutMem).

:- pred set_val(int::in, int::in, map(int, int)::in, map(int, int)::out) is det.
set_val(Value, Address, InMem, OutMem) :-
	set(Address, Value, InMem, OutMem).

:- pred get_address(list(boolx)::in, int::out) is multi.
get_address(BoolxList, Result) :-
	map(move_address, BoolxList, BoolList),
	from_padded_binary(BoolList, Result).

:- pred move_address(boolx::in, bool::out) is multi.
move_address(X, Y) :-
	X = one, Y = yes;
	X = zero, Y = no;
	X = x, (Y = yes ; Y = no).

:- pred to_boolx_list(string::in, list(boolx)::out) is det.
to_boolx_list(String, Result) :-
	to_char_list(String, CharList),
	filter_map(char_to_boolx, CharList, Result).

:- pred char_to_boolx(char::in, boolx::out) is semidet.
char_to_boolx(Char, Result) :-
	Char = '1', Result = one ;
	Char = '0', Result = zero ;
	Char = 'X', Result = x.

:- pred mask_address(list(boolx)::in, int::in, list(boolx)::out) is det.
mask_address(Mask, Value, Result) :-
	to_binary(Value, BinVal),
	pad_out(36, BinVal, PaddedBinVal),
	map_corresponding(change_char, Mask, PaddedBinVal, Result).

:- pred pad_out(int::in, list(bool)::in, list(bool)::out) is det.
pad_out(Int, BinVal, Result) :-
	length(BinVal, Length),
	( if
		Length < Int
	then
		NewBV = [no | BinVal],
		pad_out(Int, NewBV, Result)
	else
		Result = BinVal
	).

:- pred to_binary(int::in, list(bool)::out) is det.
to_binary(Int, Result) :-
	( if
		Int > 0
	then
		NextInt = Int // 2,
		Remainder = Int mod 2,
		to_binary(NextInt, Sub),
		( if Remainder = 1 then Bool = yes else Bool = no ),
		Result = Sub ++ [Bool]
	else
		Result = []
	).

:- pred from_padded_binary(list(bool)::in, int::out) is det.
from_padded_binary(List, Result) :-
	foldr2(bit_value, List, 0, Result, 0, _).

:- pred bit_value(bool::in, int::in, int::out, int::in, int::out) is det.
bit_value(Bool, InTot, OutTot, InPow, OutPow) :-
	( if Bool = yes
	then
		pow(2, InPow, X),
		OutTot = InTot + X
	
	else
		OutTot = InTot
	),
	OutPow = InPow + 1.

:- pred change_char(boolx::in, bool::in, boolx::out) is det.
change_char(MaskBit, ActualBit, Result) :-
	( if MaskBit = zero  
	then
		ActualBit = yes, Result = one ;
		ActualBit = no, Result = zero
	else
		Result = MaskBit
	).

:- pred get_sum(map(int, int)::in, int::out) is det.
get_sum(Map, Result) :-
	foldl(sum_of, Map, 0, Result).

:- pred sum_of(int::in, int::in, int::in, int::out) is det.
sum_of(_, V, A, Result) :- Result = V + A.

main(!IO) :-
	process_stream(!IO, StringInst),
	map(det_instruction_parser, StringInst, Instructions),
	foldl2(execute_instruction, Instructions, [], _, map.init, Memory),
	get_sum(Memory, Result),
	io.print(Result, !IO),
	io.nl(!IO).
