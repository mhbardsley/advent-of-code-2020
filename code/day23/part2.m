/* Advent of Code - Day 23, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module list, int, string, char, require, map, maybe.

main(!IO) :-
	io.print(Result, !IO),
	io.nl(!IO),
	convert_to_map(186524973, Map, Start, End),
	do_iterations(Map, Start, End, FinalMap),
	get_after_one(FinalMap, After1, After2),
	Result = After1 * After2.

:- pred convert_to_map(int::in, map(int, maybe(int))::out, int::out, 
	int::out) is det.
convert_to_map(Int, Map, Start, End) :-
	int_to_string(Int, String),
	to_char_list(String, CharList),
	ListWithout = list.map(det_decimal_digit_to_int, CharList),
	List = list.append(ListWithout, 10 .. 1000000),
	list_to_map(List, Map, Start, End).

:- pred list_to_map(list(int)::in, map(int, maybe(int))::out, int::out, 
	int::out) is det.
list_to_map(IntList, Map, Start, End) :-
	( if
	  	IfStart = head(IntList),
		last(IntList, IfEnd)
	then
		create_main_pointers(IntList, init, Map),
		Start = IfStart,
		End = IfEnd
	else
		error("list_to_map")
	).

:- pred create_main_pointers(list(int)::in, map(int, maybe(int))::in,
	map(int, maybe(int))::out) is det.
create_main_pointers(IntList, InMap, OutMap) :-
	( if
	  	IntList = [X, Y | Rest]
	then
		create_main_pointers([Y | Rest], NewMap, OutMap),
		det_insert(X, yes(Y), InMap, NewMap)
	else if
		IntList = [X]
	then
		det_insert(X, no, InMap, OutMap)
	else
		error("create_main_pointers")
	).

:- pred do_iterations(map(int, maybe(int))::in, int::in, int::in, 
	map(int, maybe(int))::out) is det.
do_iterations(Map, Start, End, MapOut) :-
	Indices = 0 .. 9999999,
	foldl3(single_iteration, Indices, Map, MapOut, Start, _, End, _).

:- pred single_iteration(int::in, map(int, maybe(int))::in, 
	map(int, maybe(int))::out, int::in, int::out, int::in, int::out) is det.
single_iteration(_, InMap, OutMap, InStart, OutStart, InEnd, OutEnd) :-
	get_2345(InMap, InStart, Second, Third, Fourth, Fifth),
	det_update(InStart, yes(Fifth), InMap, Map2),
	get_rest_val(InStart, Second, Third, Fourth, RestVal),
	update_last(Fourth, RestVal, InEnd, End2),
	lookup(Map2, RestVal, Value),
	det_update(Fourth, Value, Map2, Map3),
	det_update(RestVal, yes(Second), Map3, Map4),
	do_shift(Map4, OutMap, InStart, OutStart, End2, OutEnd).

:- pred get_2345(map(int, maybe(int))::in, int::in, int::out, int::out, 
	int::out, int::out) is det.
get_2345(Map, Start, Second, Third, Fourth, Fifth) :-
	( if
	  	lookup(Map, Start, Maybe1),
		lookup(Map, A, Maybe2),
		lookup(Map, B, Maybe3),
		lookup(Map, C, Maybe4),
		Maybe1 = yes(A),
		Maybe2 = yes(B),
		Maybe3 = yes(C),
		Maybe4 = yes(D)
	then
		Second = A,
		Third = B,
		Fourth = C,
		Fifth = D
	else
		error("get_2345")
	).

:- pred get_rest_val(int::in, int::in, int::in, int::in, int::out) is det.
get_rest_val(Start, Second, Third, Fourth, RestVal) :-
	TestVal = 1 + ((Start - 2) mod 1000000),
	CheckList = [Second, Third, Fourth],
	( if
	  	not member(TestVal, CheckList)
	then
		RestVal = TestVal
	else
		get_rest_val(TestVal, Second, Third, Fourth, RestVal)
	).

:- pred update_last(int::in, int::in, int::in, int::out) is det.
update_last(Fourth, RestVal, InEnd, OutEnd) :-
	( if
	  	InEnd = RestVal
	then
		OutEnd = Fourth
	else
		OutEnd = InEnd
	).
		

:- pred do_shift(map(int, maybe(int))::in, map(int, maybe(int))::out, 
	int::in, int::out, int::in, int::out) is det.
do_shift(InMap, OutMap, InStart, OutStart, InEnd, OutEnd) :-
	lookup(InMap, InStart, A),
	( if
	  	A = yes(X)
	then
		OutStart = X
	else
		error("do_shift")
	),
	det_update(InEnd, yes(InStart), InMap, Map2),
	det_update(InStart, no, Map2, OutMap),
	OutEnd = InStart.

:- pred get_after_one(map(int, maybe(int))::in, int::out, int::out) is det.
get_after_one(Map, Val1, Val2) :-
	( if
		lookup(Map, 1, V1),
	  	V1 = yes(X),
		lookup(Map, X, V2),
		V2 = yes(Y)
	then
		Val1 = X,
		Val2 = Y
	else
		error("get_after_one")
	).
