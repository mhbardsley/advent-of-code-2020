/* Advent of Code - Day 23, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module list, int, string, char, require.

main(!IO) :-
	io.print(Result, !IO),
	io.nl(!IO),
	convert_to_list(186524973, InputList),
	do_iterations(InputList, UnshiftedList),
	get_after_one(UnshiftedList, ResultList),
	convert_to_int(ResultList, Result).

:- pred convert_to_list(int::in, list(int)::out) is det.
convert_to_list(Int, List) :-
	int_to_string(Int, String),
	to_char_list(String, CharList),
	List = map(det_decimal_digit_to_int, CharList).

:- pred do_iterations(list(int)::in, list(int)::out) is det.
do_iterations(ListIn, ListOut) :-
	Indices = 0 .. 99,
	foldl(single_iteration, Indices, ListIn, ListOut).

:- pred single_iteration(int::in, list(int)::in, list(int)::out) is det.
single_iteration(_, ListIn, ListOut) :-
	do_split(ListIn, CurVal, ThreeCups, Rest),
	get_next_value(CurVal, Rest, NextVal),
	take_while(check_val(NextVal), Rest, BeforeVal, ValAndAfter),
	insert_three(CurVal, BeforeVal, ThreeCups, ValAndAfter, NewList),
	shift_left_once(NewList, ListOut).

:- pred do_split(list(int)::in, int::out, list(int)::out, 
	list(int)::out) is det.
do_split(ListIn, CurVal, ThreeCups, Rest) :-
	( if
		ListIn = [A, B, C, D | Xs]
	then
		CurVal = A,
		ThreeCups = [B, C, D],
		Rest = Xs
	else
		error("do_split")
	).

:- pred get_next_value(int::in, list(int)::in, int::out) is det.
get_next_value(CurVal, Rest, NextVal) :-
/*	trace [io(!IO)]
	(
		io.print(CurVal, !IO),
		io.nl(!IO),
		io.print(Rest, !IO),
		io.nl(!IO),
		io.nl(!IO)
	),
*/
	length(Rest, LengthMinusFour),
	Length = LengthMinusFour + 4,
	TestVal = 1 + ((CurVal - 2) mod Length),
	( if
		member(TestVal, Rest)
	then
		NextVal = TestVal
	else
		get_next_value(TestVal, Rest, NextVal)
	).

:- pred check_val(int::in, int::in) is semidet.
check_val(NextVal, ValToCheck) :-
	ValToCheck \= NextVal.

:- pred insert_three(int::in, list(int)::in, list(int)::in, 
	list(int)::in, list(int)::out) is det.
insert_three(CurVal, BeforeVal, ThreeCups, ValAndAfter, Result) :-
	( if
		ValAndAfter = [Val | After]
	then
		Result = [CurVal] ++ BeforeVal ++ [Val] ++ ThreeCups ++ After
	else
		error("insert_three")
	).

:- pred shift_left_once(list(int)::in, list(int)::out) is det.
shift_left_once(ListIn, ListOut) :-
	( if
		ListIn = [X | Rest]
	then
		ListOut = Rest ++ [X]
	else
		error("shift_left_once")
	).

:- pred get_after_one(list(int)::in, list(int)::out) is det.
get_after_one(ListIn, ListOut) :-
	( if
		ListIn = [1 | Rest]
	then
		ListOut = Rest
	else
		shift_left_once(ListIn, Temp),
		get_after_one(Temp, ListOut)
	).

:- pred convert_to_int(list(int)::in, int::out) is det.
convert_to_int(IntList, Result) :-
	map(int_to_string, IntList, StringList),
	append_list(StringList, String),
	Result = det_to_int(String).
