/* Advent of Code - Day 25, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is cc_multi.
:- implementation.
:- import_module input_processor, list, int, require, string.

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	to_pks(StringRep, PK1, PK2),
	( if
		get_sk(PK1, 1, 1, SK1),
		get_sk(PK2, 1, 1, SK2)
	then
		encryption_key(PK1, PK2, SK1, SK2, Result)
	else
		error("Could not find secret keys")
	).

:- pred to_pks(list(string)::in, int::out, int::out) is det.
to_pks(StringRep, PK1, PK2) :-
	PKs = map(det_to_int, StringRep),
	( if
		PKs = [X, Y]
	then
		PK1 = X, PK2 = Y
	else
		error("More than two public keys given")
	).

:- pred get_sk(int::in, int::in, int::in, int::out) is nondet.
get_sk(PK, ValueIn, SKIn, SKOut) :-
	PK = ValueIn, SKOut = SKIn - 1
	;
	SKIn < 20201227,
	NewValue = (ValueIn * 7) mod 20201227,
	get_sk(PK, NewValue, SKIn + 1, SKOut).

:- pred get_pk(int::in, int::in, int::out) is det.
get_pk(SubjectNo, LoopSize, Result) :-
	foldl(run_loop(SubjectNo), 0 .. LoopSize - 1, 1, Result).

:- pred run_loop(int::in, int::in, int::in, int::out) is det.
run_loop(SubjectNo, _, Value, NewValue) :-
	NewValue = (Value * SubjectNo) mod 20201227.

:- pred encryption_key(int::in, int::in, int::in, int::in, int::out) is det.
encryption_key(PK1, PK2, SK1, SK2, Result) :-
	get_pk(PK1, SK2, Res1),
	get_pk(PK2, SK1, Res2),
	( if
		Res1 = Res2
	then
		Result = Res1
	else
		error("Non-matching encryption keys!")
	).
