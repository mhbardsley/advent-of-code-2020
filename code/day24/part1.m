/* Advent of Code - Day 24, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, set, int, math, char, pair, 
	string, require.

:- type action
	--->	e
	;	se
	;	sw
	;	w
	;	nw
	;	ne.

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	to_actions(StringRep, Actions),
	foldl(do_actions, Actions, init, BlackTileCoords),
	IterationsToRun = 0 .. 99,
	foldl(run_iteration, IterationsToRun, BlackTileCoords, FinalBlackTiles),
	count(FinalBlackTiles, Result).

:- pred to_actions(list(string)::in, list(list(action))::out) is det.
to_actions(Strings, Result) :-
	map(line_to_actions, Strings, Result).

:- pred line_to_actions(string::in, list(action)::out) is det.
line_to_actions(String, Result) :-
	to_char_list(String, CharList),
	char_list_actions(CharList, Result).

:- pred char_list_actions(list(char)::in, list(action)::out) is det.
char_list_actions(CharList, Actions) :-
	( if
		CharList = []
	then
		Actions = []
	else
		take_while(not_ew, CharList, Start, End),
		( if
			End = [X | Rest]
		then
			from_char_list(Start ++ [X], CompassPoint),
			CPDot = CompassPoint ++ ".",
			( if
				io.read_from_string("args", CPDot, 
					length(CPDot), Action, 
					io.posn(0, 0, 0), _),
				Action = ok(CP)
			then
				char_list_actions(Rest, OtherActions),
				Actions = [CP | OtherActions]
			else
				error("Issue reading strings to terms")
			)
		else
			error("End is empty!")
		)
	).

:- pred not_ew(char::in) is semidet.
not_ew(Char) :-
	Char \= 'e', Char \= 'w'.

:- pred do_actions(list(action)::in, set(pair(int, int))::in,
	set(pair(int, int))::out) is det.
do_actions(Actions, InSet, OutSet) :-
	foldl2(process_action, Actions, 0, X, 0, Y),
	Pair = pair(X, Y),
	( if
		member(Pair, InSet)
	then
		delete(Pair, InSet, OutSet)
	else
		insert(Pair, InSet, OutSet)
	).

:- pred process_action(action::in, int::in, int::out, int::in, 
	int::out) is det.
process_action(Action, XIn, XOut, YIn, YOut) :-
	( if
		Action = e
	then
		XOut = XIn + 2,
		YOut = YIn
	else if
		Action = se
	then
		XOut = XIn + 1,
		YOut = YIn - 1
	else if
		Action = sw
	then
		XOut = XIn - 1,
		YOut = YIn - 1
	else if
		Action = w
	then
		XOut = XIn - 2,
		YOut = YIn
	else if
		Action = nw
	then
		XOut = XIn - 1,
		YOut = YIn + 1
	else
		XOut = XIn + 1,
		YOut = YIn + 1
	).

:- pred run_iteration(int::in, set(pair(int, int))::in, 
	set(pair(int, int))::out) is det.
run_iteration(_, InBlackCoords, OutBlackCoords) :-
	get_remaining_blacks(InBlackCoords, RemainingBlacks),
	get_candidate_whites(InBlackCoords, CandidateWhites),
	get_new_blacks(CandidateWhites, InBlackCoords, NewBlacks),
	union(RemainingBlacks, NewBlacks, OutBlackCoords).

:- pred get_remaining_blacks(set(pair(int, int))::in, 
	set(pair(int, int))::out) is det.
get_remaining_blacks(InBlackCoords, OutBlackCoords) :-
	filter(is_remaining_black(InBlackCoords), InBlackCoords, 
		OutBlackCoords).

:- pred is_remaining_black(set(pair(int, int))::in, pair(int, int)::in) 
	is semidet.
is_remaining_black(BlackCoords, Pair) :-
	some [Black1, Black2]
	(
		Black1 \= Black2,
		Black1 \= Pair,
		Black2 \= Pair,
		member(Black1, BlackCoords),
		member(Black2, BlackCoords),
		(
			are_neighbours(Pair, Black1) 
		;
			are_neighbours(Pair, Black2)
		),
		all [OtherBlack]
		(
			(not member(OtherBlack, [Black1, Black2, Pair]),
			member(OtherBlack, BlackCoords)) 
			=>
			not are_neighbours(Pair, OtherBlack)
		)
	).

:- pred are_neighbours(pair(int, int)::in, pair(int, int)::in) is semidet.
are_neighbours(Pair1, Pair2) :-
	fst(Pair1, X1),
	snd(Pair1, Y1),
	fst(Pair2, X2),
	snd(Pair2, Y2),
	abs(X2 - X1) + abs(Y2 - Y1) = 2.

:- pred get_candidate_whites(set(pair(int, int))::in, 
	set(pair(int, int))::out) is det.
get_candidate_whites(BlackCoords, WhiteCandidates) :-
	solutions_set(get_a_white(BlackCoords), AllPossibleWhites),
	difference(AllPossibleWhites, BlackCoords, WhiteCandidates).

:- pred get_a_white(set(pair(int, int))::in, pair(int, int)::out) is nondet.
get_a_white(BlackCoords, WhitePair) :-
	foldl2(update_min_max, BlackCoords, pair(max_int, max_int), 
		Min, pair(min_int, min_int), Max),
	MinX = fst(Min),
	MinY = snd(Min),
	MaxX = fst(Max),
	MaxY = snd(Max),
	member(PossX, MinX - 2 .. MaxX + 2),
	member(PossY, MinY - 2 .. MaxY + 2),
	WhitePair = pair(PossX, PossY).

:- pred update_min_max(pair(int, int)::in, pair(int, int)::in, 
	pair(int, int)::out, pair(int, int)::in, pair(int, int)::out) is det.
update_min_max(BlackCoords, MinsIn, MinsOut, MaxsIn, MaxsOut) :-
	fst(BlackCoords, BlackX),
	snd(BlackCoords, BlackY),
	fst(MinsIn, MinX),
	snd(MinsIn, MinY),
	fst(MaxsIn, MaxX),
	snd(MaxsIn, MaxY),
	MinsOut = pair(min(BlackX, MinX), min(BlackY, MinY)),
	MaxsOut = pair(max(BlackX, MaxX), min(BlackY, MaxY)).

:- pred get_new_blacks(set(pair(int, int))::in, set(pair(int,int))::in, 
	set(pair(int, int))::out) is det.
get_new_blacks(CandidateWhites, BlackCoords, NewBlacks) :-
	filter(is_new_black(BlackCoords), CandidateWhites, NewBlacks).

:- pred is_new_black(set(pair(int, int))::in, pair(int, int)::in) is semidet.
is_new_black(BlackCoords, CandidateWhite) :-
	some [Black1, Black2]
	(
		member(Black1, BlackCoords),
		member(Black2, BlackCoords),
		Black1 \= Black2,
		are_neighbours(CandidateWhite, Black1),
		are_neighbours(CandidateWhite, Black2),
		all [OtherBlack]
		(
			(not member(OtherBlack, [Black1, Black2]),
			member(OtherBlack, BlackCoords))
			=>
			not are_neighbours(CandidateWhite, OtherBlack)
		)
	).
