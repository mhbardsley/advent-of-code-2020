/* Advent of Code - Day 24, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, set, int, math, char, pair, 
	string, require, solutions.

:- type action
	--->	e
	;	se
	;	sw
	;	w
	;	nw
	;	ne.

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	to_actions(StringRep, Actions),
	foldl(do_actions, Actions, init, BlackTileCoords),
	IterationsToRun = 0 .. 99,
	foldl(run_iteration, IterationsToRun, BlackTileCoords, FinalBlackTiles),
	count(FinalBlackTiles, Result).

:- pred to_actions(list(string)::in, list(list(action))::out) is det.
to_actions(Strings, Result) :-
	map(line_to_actions, Strings, Result).

:- pred line_to_actions(string::in, list(action)::out) is det.
line_to_actions(String, Result) :-
	to_char_list(String, CharList),
	char_list_actions(CharList, Result).

:- pred char_list_actions(list(char)::in, list(action)::out) is det.
char_list_actions(CharList, Actions) :-
	( if
		CharList = []
	then
		Actions = []
	else
		take_while(not_ew, CharList, Start, End),
		( if
			End = [X | Rest]
		then
			from_char_list(Start ++ [X], CompassPoint),
			CPDot = CompassPoint ++ ".",
			( if
				io.read_from_string("args", CPDot, 
					length(CPDot), Action, 
					io.posn(0, 0, 0), _),
				Action = ok(CP)
			then
				char_list_actions(Rest, OtherActions),
				Actions = [CP | OtherActions]
			else
				error("Issue reading strings to terms")
			)
		else
			error("End is empty!")
		)
	).

:- pred not_ew(char::in) is semidet.
not_ew(Char) :-
	Char \= 'e', Char \= 'w'.

:- pred do_actions(list(action)::in, set(pair(int, int))::in,
	set(pair(int, int))::out) is det.
do_actions(Actions, InSet, OutSet) :-
	foldl2(process_action, Actions, 0, X, 0, Y),
	Pair = pair(X, Y),
	( if
		member(Pair, InSet)
	then
		delete(Pair, InSet, OutSet)
	else
		insert(Pair, InSet, OutSet)
	).

:- pred process_action(action::in, int::in, int::out, int::in, 
	int::out) is det.
process_action(Action, XIn, XOut, YIn, YOut) :-
	( if
		Action = e
	then
		XOut = XIn + 2,
		YOut = YIn
	else if
		Action = se
	then
		XOut = XIn + 1,
		YOut = YIn - 1
	else if
		Action = sw
	then
		XOut = XIn - 1,
		YOut = YIn - 1
	else if
		Action = w
	then
		XOut = XIn - 2,
		YOut = YIn
	else if
		Action = nw
	then
		XOut = XIn - 1,
		YOut = YIn + 1
	else
		XOut = XIn + 1,
		YOut = YIn + 1
	).

:- pred run_iteration(int::in, set(pair(int, int))::in, 
	set(pair(int, int))::out) is det.
run_iteration(_, InBlackCoords, OutBlackCoords) :-
	get_candidates(InBlackCoords, Candidates),
	get_new_blacks(Candidates, InBlackCoords, OutBlackCoords).

:- pred are_neighbours(pair(int, int), pair(int, int)).
:- mode are_neighbours(in, out) is multi.
:- mode are_neighbours(in, in) is semidet.
are_neighbours(Pair1, Pair2) :-
	Pair1 = (X1 - Y1),
	Pair2 = (X2 - Y2),
	(
		X1 = X2 + 2, Y1 = Y2
	;
		X1 = X2 + 1, Y1 = Y2 - 1
	;
		X1 = X2 - 1, Y1 = Y2 - 1
	;
		X1 = X2 - 2, Y1 = Y2
	;
		X1 = X2 - 1, Y1 = Y2 + 1
	;
		X1 = X2 + 1, Y1 = Y2 + 1
	).
:- pred are_neighbours_plus(pair(int, int)::in, pair(int, int)::out) is multi.
are_neighbours_plus(Pair1, Pair2) :-
	are_neighbours(Pair1, Pair2) ; Pair2 = Pair1.

:- pred are_neighbours_multi(pair(int, int)::in, pair(int, int)::out) is multi.
are_neighbours_multi(Pair1, Pair2) :-
	are_neighbours(Pair1, Pair2).

:- pred get_candidates(set(pair(int, int))::in, set(pair(int, int))::out)
	is det.
get_candidates(BlackCoords, Candidates) :-
	solutions_set(get_a_candidate(BlackCoords), Candidates).

:- pred get_a_candidate(set(pair(int, int))::in, pair(int, int)::out) is nondet.
get_a_candidate(BlackCoords, Candidate) :-
	some [BlackCoord]
	(
		member(BlackCoord, BlackCoords),
		are_neighbours_plus(BlackCoord, Candidate)
	).

:- pred get_new_blacks(set(pair(int, int))::in, set(pair(int,int))::in, 
	set(pair(int, int))::out) is det.
get_new_blacks(Candidates, BlackCoords, NewBlacks) :-
	filter(is_new_black(BlackCoords), Candidates, NewBlacks).

:- pred is_new_black(set(pair(int, int))::in, pair(int, int)::in) is semidet.
is_new_black(BlackCoords, Candidate) :-
	solutions_set(are_neighbours_multi(Candidate), Neighbours),
	filter(contains(BlackCoords), Neighbours, BlackNeighbours),
	count(BlackNeighbours, NoOf),
	(NoOf = 2 ; NoOf = 1, member(Candidate, BlackCoords)).
