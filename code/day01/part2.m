/* Advent of Code: Day 1, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module int, string, list, solutions.

:- pred sums(list(int)::in, int::in, int::out) is nondet.
sums(Values, Target, Result) :-
	member(A, Values),
	member(B, Values),
	member(C, Values),
	Target = A + B + C,
	Result = A * B * C.


:- pred to_list(io.text_input_stream::in, list(int)::out, io::di, io::uo) is det.
to_list(Stream, Xs, !IO) :-
	io.read_line_as_string(Stream, Result, !IO),
	( if 
		Result = ok(String), 
		X = chomp(String), 
		to_int(X, XInt) 
	then
		to_list(Stream, Rest, !IO),
		Xs = [XInt | Rest]
	else
		Xs = []
	).

main(!IO) :-
	io.open_input("input", ResStream, !IO),
	( if
		ResStream = ok(Stream)
	then
		to_list(Stream, List, !IO),
		solutions(sums(List, 2020), Solutions),
		( if 
			Solutions = []
		then
			io.write_string("No possible answers.", !IO)
		else
			io.print(Solutions, !IO)
		)
	else
		io.write_string("Issue with input stream.", !IO)
	),
	io.nl(!IO).
