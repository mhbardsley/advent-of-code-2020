/* Advent of Code 2020 - Day 18, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int, char.

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	tokenize(StringRep, Tokens),
	get_values(Tokens, Values),
	sum_result(Values, Result).

:- pred tokenize(list(string)::in, list(list(string))::out) is det.
tokenize(StringRep, Equations) :-
	( if
		StringRep = [String | Rest]
	then
		to_char_list(String, CharList),
		convert_to_tokens(CharList, [], Tokens),
		tokenize(Rest, OtherEquations),
		Equations = [Tokens | OtherEquations]
	else
		Equations = []
	).

:- pred convert_to_tokens(list(char)::in, list(char)::in, list(string)::out) is det.
convert_to_tokens(CharList, Current, Tokens) :-
/*	trace [io(!IO)]
	(
	 	io.print(CharList, !IO),
		io.nl(!IO),
		io.print(Current, !IO),
		io.nl(!IO),
		io.nl(!IO)
	),
*/
	( if
	  	CharList = [Char | Chars]
	then
		( if
		  	Char = ' ',
			Current \= []
		then
			convert_to_tokens(Chars, [], Others),
			Tokens = [from_char_list(Current) | Others]
		else if
			Char = ' ',
			Current = []
		then
			convert_to_tokens(Chars, [], Tokens)
		else if
			(Char = '(' ; Char = ')'),
			Current \= []
		then
			convert_to_tokens(Chars, [], Others),
			Tokens = [from_char_list(Current), char_to_string(Char) | Others]
		else if
			(Char = '(' ; Char = ')'),
			Current = []
		then
			convert_to_tokens(Chars, [], Others),
			Tokens = [char_to_string(Char) | Others]
		else
			New = Current ++ [Char],
			convert_to_tokens(Chars, New, Tokens)
		)
	else
		if (Current = [])
		then Tokens = []
		else Tokens = [from_char_list(Current)]
	).

:- pred get_values(list(list(string))::in, list(int)::out) is det.
get_values(Tokens, Results) :-
/*	trace [io(!IO)]
	(
	 	io.print(Tokens, !IO),
		io.nl(!IO)
	),
*/
	( if
		Tokens = [Token | Rest]
	then
		( if
			get_value(Token, Result)
		then
			Results = [Result | OtherResults]
		else
			Results = OtherResults
		),
		get_values(Rest, OtherResults)
	else
		Results = []
	).

:- pred get_value(list(string)::in, int::out) is semidet.
get_value(Tokens, Result) :-
/*	trace [io(!IO)]
	(
	 	io.print(Tokens, !IO),
		io.nl(!IO)
	),
*/
	(
	Tokens = [], Result = 0
	;
	Tokens = [X | _],
	(X = "(" ; to_int(X, _)), 
	expr(Tokens, TokensOut, SubRes),
	rest(TokensOut, SubRes, _, Result)
	).

:- pred expr(list(string)::in, list(string)::out, int::out) is semidet.
expr(TokensIn, TokensOut, Result) :-
/*	trace [io(!IO)]
	(
	 	io.print(TokensIn, !IO),
		io.nl(!IO),
		io.print(TokensOut, !IO),
		io.nl(!IO),
		io.print(Result, !IO),
		io.nl(!IO)
	),
*/
	( if
	  	TokensIn = ["(" | Rest]
	then
		expr(Rest, Remaining, SubRes),
		rest(Remaining, SubRes, ActualRemaining, Result),
		head(ActualRemaining) = ")",
		TokensOut = tail(ActualRemaining)
	else
		TokensIn = [X | TokensOut],
		to_int(X, Result)
	).

:- pred rest(list(string)::in, int::in, list(string)::out, int::out) is semidet.
rest(TokensIn, IntRep, TokensOut, Result) :-
/*	trace [io(!IO)]
	(
	 	io.print(TokensIn, !IO),
		io.nl(!IO),
		io.print(IntRep, !IO),
		io.nl(!IO),
		io.nl(!IO)
	),
*/
	( if
		is_op(head(TokensIn))
	then
		IntRep = Op1,
		expr(tail(TokensIn), Remaining, Op2),
		calculate(Op1, Op2, head(TokensIn), SubRes),
		rest(Remaining, SubRes, TokensOut, Result)
	else
		(is_empty(TokensIn) ; head(TokensIn) = ")"),
		Result = IntRep,
		TokensOut = TokensIn
	).

:- pred is_op(string::in) is semidet.
is_op(StringOp) :-
	StringOp = "*" ; StringOp = "/" ; StringOp = "-" ; StringOp = "+".

:- pred calculate(int::in, int::in, string::in, int::out) is det.
calculate(Op1, Op2, StringOp, Result) :-
	( if
	  	StringOp = "*"
	then
		Result = Op1 * Op2
	else if
		StringOp = "/"
	then
		Result = Op1 / Op2
	else if
		StringOp = "+"
	then
		Result = Op1 + Op2
	else
		Result = Op1 - Op2
	).

:- pred sum_result(list(int)::in, int::out) is det.
sum_result(Results, Result) :-
	Result = foldl(plus, Results, 0).
