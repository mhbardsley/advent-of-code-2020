/* Advent of Code 2020 - Day 18, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int, char.

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	tokenize(StringRep, Tokens),
	get_values(Tokens, Values),
	sum_result(Values, Result).

:- pred tokenize(list(string)::in, list(list(string))::out) is det.
tokenize(StringRep, Equations) :-
	( if
		StringRep = [String | Rest]
	then
		to_char_list(String, CharList),
		convert_to_tokens(CharList, [], Tokens),
		tokenize(Rest, OtherEquations),
		Equations = [Tokens | OtherEquations]
	else
		Equations = []
	).

:- pred convert_to_tokens(list(char)::in, list(char)::in, list(string)::out) is det.
convert_to_tokens(CharList, Current, Tokens) :-
	( if
	  	CharList = [Char | Chars]
	then
		( if
		  	Char = ' ',
			Current \= []
		then
			convert_to_tokens(Chars, [], Others),
			Tokens = [from_char_list(Current) | Others]
		else if
			Char = ' ',
			Current = []
		then
			convert_to_tokens(Chars, [], Tokens)
		else if
			(Char = '(' ; Char = ')'),
			Current \= []
		then
			convert_to_tokens(Chars, [], Others),
			Tokens = [from_char_list(Current), char_to_string(Char) | Others]
		else if
			(Char = '(' ; Char = ')'),
			Current = []
		then
			convert_to_tokens(Chars, [], Others),
			Tokens = [char_to_string(Char) | Others]
		else
			New = Current ++ [Char],
			convert_to_tokens(Chars, New, Tokens)
		)
	else
		if (Current = [])
		then Tokens = []
		else Tokens = [from_char_list(Current)]
	).

:- pred get_values(list(list(string))::in, list(int)::out) is det.
get_values(Tokens, Results) :-
	( if
		Tokens = [Token | Rest]
	then
		( if
			mult(Token, _, Result)
		then
			Results = [Result | OtherResults]
		else
			Results = OtherResults
		),
		get_values(Rest, OtherResults)
	else
		Results = []
	).

:- pred mult(list(string)::in, list(string)::out, int::out) is semidet.
mult(Tokens, OutTokens, Result) :-
	add(Tokens, Remaining, SubRes),
	mult_p(Remaining, SubRes, OutTokens, Result).

:- pred mult_p(list(string)::in, int::in, list(string)::out, int::out) is semidet.
mult_p(Tokens, SubRes, OutTokens, Result) :-
	Tokens = [Head | Tail],
	( if
	  	Head = "*"
	then
		add(Tail, Remaining, Sub),
		NextRes = SubRes * Sub,
		mult_p(Remaining, NextRes, OutTokens, Result)
	else if
		Head = "/"
	then
		add(Tail, Remaining, Sub),
		NextRes = SubRes / Sub,
		mult_p(Remaining, NextRes, OutTokens, Result)
	else
		Head = ")",
		OutTokens = Tokens,
		Result = SubRes
	)
	;
	Tokens = [], OutTokens = Tokens, Result = SubRes.

:- pred add(list(string)::in, list(string)::out, int::out) is semidet.
add(Tokens, OutTokens, Result) :-
	term(Tokens, Remaining, SubRes),
	add_p(Remaining, SubRes, OutTokens, Result).

:- pred add_p(list(string)::in, int::in, list(string)::out, int::out) is semidet.
add_p(Tokens, SubRes, OutTokens, Result) :-
	Tokens = [Head | Tail],
	( if
	  	Head = "+"
	then
		term(Tail, Remaining, Sub),
		NextRes = SubRes + Sub,
		add_p(Remaining, NextRes, OutTokens, Result)
	else if
		Head = "-"
	then
		term(Tail, Remaining, Sub),
		NextRes = SubRes - Sub,
		add_p(Remaining, NextRes, OutTokens, Result)
	else
		(Head = ")" ; Head = "*" ; Head = "/"),
		OutTokens = Tokens,
		Result = SubRes
	)
	;
	Tokens = [], OutTokens = Tokens, Result = SubRes.

:- pred term(list(string)::in, list(string)::out, int::out) is semidet.
term(Tokens, OutTokens, Result) :-
	( if
	  	to_int(head(Tokens), _)
	then
		Tokens = [Head | OutTokens],
		to_int(Head, Result)
	else
		Tokens = ["(" | Rest],
		mult(Rest, Remaining, Result),
		Remaining = [")" | OutTokens]
	).

:- pred sum_result(list(int)::in, int::out) is det.
sum_result(Results, Result) :-
	Result = foldl(plus, Results, 0).
