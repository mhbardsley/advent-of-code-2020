/* Advent of Code - Day 16, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int.

:- type limit
	--->	limit(
			lower :: int,
			upper :: int
		).

main(!IO) :-
	process_stream(!IO, StringRes),
	io.print(Result, !IO),
	io.nl(!IO),
	extract_info(StringRes, Limits, Values),
	get_non_compliants(Limits, Values, NonCompliants),
	sum(NonCompliants, Result).

:- pred extract_info(list(string)::in, list(limit)::out, list(int)::out) is det.
extract_info(StringRes, Limits, Values) :-
	( if
	  	StringRes = [X | Xs]
	then
		( if
		  	X = "",
			StringRes = [_, _, _, _, _ | Next]
		then
			extract_info(Next, Limits, Values)
		else if
			get_limits(X, Limit1, Limit2)
		then
			extract_info(Xs, OtherLims, Values),
			Limits = [Limit1, Limit2 | OtherLims]
		else if 
			get_ticket(X, Ticket)
		then
			extract_info(Xs, Limits, OtherVals),
			Values = Ticket ++ OtherVals
		else
			extract_info(Xs, Limits, Values)
		)
	else
		Limits = [],
		Values = []
	).

:- pred get_limits(string::in, limit::out, limit::out) is semidet.
get_limits(Line, Limit1, Limit2) :-
	Words = words(Line),
	Words = [_, L1, _, L2],
	[Lower1Str, Upper1Str] = split_at_char('-', L1),
	[Lower2Str, Upper2Str] = split_at_char('-', L2),
	to_int(Lower1Str, Lower1),
	to_int(Upper1Str, Upper1),
	to_int(Lower2Str, Lower2),
	to_int(Upper2Str, Upper2),
	Limit1 = limit(Lower1, Upper1),
	Limit2 = limit(Lower2, Upper2).

:- pred get_ticket(string::in, list(int)::out) is semidet.
get_ticket(Line, Values) :-
	StringList = split_at_char(',', Line),
	map(to_int, StringList, Values).

:- pred get_non_compliants(list(limit)::in, list(int)::in, list(int)::out) is det.
get_non_compliants(Limits, Values, NonCompliants) :-
	filter(not_complies(Limits), Values, NonCompliants).

:- pred not_complies(list(limit)::in, int::in) is semidet.
not_complies(Limits, Val) :-
	Limits = [] ;
	(
	 	Limits = [X | Xs],
		limit_noncompliant(X, Val),
		not_complies(Xs, Val)
	).

:- pred limit_noncompliant(limit::in, int::in) is semidet.
limit_noncompliant(Limit, Val) :-
	Limit = limit(L, U),
	(Val < L ; Val > U).


:- pred sum(list(int)::in, int::out) is det.
sum(NonCompliants, Result) :-
	Result = foldl(plus, NonCompliants, 0).
