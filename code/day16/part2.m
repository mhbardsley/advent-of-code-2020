/* Advent of Code - Day 16, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int, solutions, random, float, random.sfc64, math.

:- type limit
	--->	limit(
			lower :: int,
			upper :: int
		).

:- type field
	--->	field(
			field_name :: string,
			lower_limit :: limit,
			upper_limit :: limit
		).

main(!IO) :-
	process_stream(!IO, StringRes),
	extract_info(StringRes, Fields, MyTicket, TheirTickets),
	filter_compliants(Fields, TheirTickets, Compliants),
%	sort_fields(Fields, SortedFields),
	get_field_order(Fields, Compliants, FieldOrder, !IO),
	get_product_of_departures(MyTicket, FieldOrder, Result),
	io.print(Result, !IO),
	io.nl(!IO).

:- pred extract_info(list(string)::in, list(field)::out, list(int)::out, list(list(int))::out) is det.
extract_info(StringRes, Fields, MyTicket, TheirTickets) :-
	( if
	  	StringRes = [X | Xs]
	then
		( if
		  	X = "",
			StringRes = [_, _, MyVals, _, _ | Next],
			get_ticket(MyVals, MT)
		then
			extract_info(Next, Fields, _, TheirTickets),
			MyTicket = MT
		else if
			get_field(X, Field)
		then
			extract_info(Xs, OtherFields, MyTicket, TheirTickets),
			Fields = [Field | OtherFields]
		else if 
			get_ticket(X, Ticket)
		then
			extract_info(Xs, Fields, MyTicket, OtherTickets),
			TheirTickets = [Ticket | OtherTickets]
		else
			extract_info(Xs, Fields, MyTicket, TheirTickets)
		)
	else
		Fields = [],
		MyTicket = [],
		TheirTickets = []
	).

:- pred get_field(string::in, field::out) is semidet.
get_field(Line, Field) :-
	[Name, L1AndL2] = split_at_string(": ", Line),
	[L1, _, L2] = words(L1AndL2),
	[Lower1Str, Upper1Str] = split_at_char('-', L1),
	[Lower2Str, Upper2Str] = split_at_char('-', L2),
	to_int(Lower1Str, Lower1),
	to_int(Upper1Str, Upper1),
	to_int(Lower2Str, Lower2),
	to_int(Upper2Str, Upper2),
	Limit1 = limit(Lower1, Upper1),
	Limit2 = limit(Lower2, Upper2),
	Field = field(Name, Limit1, Limit2).

:- pred get_ticket(string::in, list(int)::out) is semidet.
get_ticket(Line, Values) :-
	StringList = split_at_char(',', Line),
	map(to_int, StringList, Values).

:- pred filter_compliants(list(field)::in, list(list(int))::in, list(list(int))::out) is det.
filter_compliants(Fields, TheirTickets, Compliants) :-
	filter(is_compliant(Fields), TheirTickets, Compliants).

:- pred is_compliant(list(field)::in, list(int)::in) is semidet.
is_compliant(Fields, Ticket) :-
	Ticket = [] ;
	(
		Ticket = [X | Xs],
		some [Y]
		(
			member(Y, Fields),
			Y = field(_, limit(L1, U1), limit(L2, U2)),
			((X >= L1, X =< U1) ; (X >= L2, X =< U2))
		),
		is_compliant(Fields, Xs)
	).

/*:- pred sort_fields(list(field)::in, list(field)::out) is det.
sort_fields(Fields, SortedFields) :-
	merge_sort(Fields, 0, length(Fields) - 1, SortedFields).

:- pred merge_sort(list(field)::in, int::in, int::in, list(field)::out) is det.
merge_sort(Fields, L, R, Result) :-
	trace [io(!IO)]
	(
		io.print(Fields, !IO),
		io.nl(!IO),
		io.print(L, !IO),
		io.nl(!IO),
		io.print(R, !IO),
		io.nl(!IO)
	),
	( if
		L < R
	then
		M = (L + R) // 2,
		merge_sort(Fields, L, M, R1),
		merge_sort(Fields, M + 1, R, R2),
		merge_fields(R1, R2, Result)
	else
		det_index0(Fields, L, Val),
		Result = [Val]
	).

:- pred merge_fields(list(field)::in, list(field)::in, list(field)::out) is det.
merge_fields(R1, R2, Result) :-
	( if
		R1 = []
	then
		Result = R2
	else if
		R2 = []
	then
		Result = R1
	else
		R1Head = det_head(R1),
		R1Head = field(_, limit(R1L1, R1U1), limit(R1U2, R1L2)),
		R2Head = det_head(R2),
		R2Head = field(_, limit(R2L1, R2U1), limit(R2U2, R2L2)),
		R1Tail = det_tail(R1),
		R2Tail = det_tail(R2),
		( if
			(R1U1 - R1L1) + (R1U2 - R1L2) <
			(R2U1 - R2L1) + (R2U2 - R2L2)
		then
			merge_fields(R1Tail, R2, SubRes),
			Result = [R1Head | SubRes]
		else
			merge_fields(R2Tail, R1, SubRes),
			Result = [R2Head | SubRes]
		)
	).
*/
:- pred get_field_order(list(field)::in, list(list(int))::in, list(field)::out, io::di, io::uo) is det.
get_field_order(Fields, TheirTickets, FieldOrder, !IO) :-
	transpose(TheirTickets, Columns),
	sfc64.init(P, S),
	make_io_urandom(P, S, M, !IO),
	find_solution(Fields, Columns, 10.0, FieldOrder, M, !IO).

:- pred transpose(list(list(int))::in, list(list(int))::out) is det.
transpose(TheirTickets, Columns) :-
	( if
		map(pseudo_head, TheirTickets, Row),
		map(pseudo_tail, TheirTickets, Rest)
	then
		transpose(Rest, RemainingRows),
		Columns = [Row | RemainingRows]
	else
		Columns = []
	).

:- pred pseudo_head(list(int)::in, int::out) is semidet.
pseudo_head(List, Result) :-
	List = [Result | _].

:- pred pseudo_tail(list(int)::in, list(int)::out) is semidet.
pseudo_tail(List, Result) :-
	List = [_ | Result].

:- pred find_solution(list(field)::in, list(list(int))::in, float::in, list(field)::out, M::in, io::di, io::uo) is det <= urandom(M, io).
find_solution(Fields, Columns, Temp, Result, M, !IO) :-
	( if
		all_conforms(Fields, Columns)
	then
		Result = Fields
	else
		get_next_fields(Fields, Columns, Temp, NextFields, M, !IO),
		find_solution(NextFields, Columns, Temp*0.99, Result, M, !IO)	
	).

:- pred all_conforms(list(field)::in, list(list(int))::in) is semidet.
all_conforms(Fields, Columns) :-
	Fields = [] ; Columns = [] ;
	(
		Fields = [X | Xs],
		Columns = [Y | Ys],
		conforms(Y, X),
		all_conforms(Xs, Ys)
	).
		

:- pred conforms(list(int)::in, field::in) is semidet.
conforms(TicketVals, Field) :-
	Field = field(_, limit(L1, U1), limit(L2, U2)),
	(TicketVals = [] ;
	(
		TicketVals = [X | Xs],
		(X >= L1, X =< U1 ; X >= L2, X =< U2),
		conforms(Xs, Field)
	)).

:- pred get_next_fields(list(field)::in, list(list(int))::in, float::in, list(field)::out, M::in, io::di, io::uo) is det <= urandom(M, io).
get_next_fields(Fields, Columns, Temperature, NextFields, M, !IO) :-
	missing_fields(Fields, Columns, CurrCost),
	switch_fields(Fields, PotentiallyNew, M, !IO),
	missing_fields(PotentiallyNew, Columns, NewCost),
	uniform_float_in_01(M, N, !IO),
	( if
		should_change(CurrCost, NewCost, Temperature, N)
	then
		NextFields = PotentiallyNew
	else
		NextFields = Fields
	).
	
:- pred missing_fields(list(field)::in, list(list(int))::in, int::out) is det.
missing_fields(Fields, Columns, Result) :-
	( if
		Fields = [X | Xs],
		Columns = [Y | Ys]
	then
		( if
			conforms(Y, X)
		then
			missing_fields(Xs, Ys, Result)
		else
			missing_fields(Xs, Ys, SubRes),
			Result = SubRes + 1
		)
	else
		Result = 0
	).

:- pred switch_fields(list(field)::in, list(field)::out, M::in, io::di, io::uo) is det <= urandom(M, io).
switch_fields(Fields, Result, M, !IO) :-
	length(Fields, FieldsLength),
	uniform_int_in_range(M, 1, FieldsLength, A, !IO),
	uniform_int_in_range(M, 1, FieldsLength, B, !IO),
	det_index1(Fields, A, ItemA),
	det_index1(Fields, B, ItemB),
	det_replace_nth(Fields, A, ItemB, Temp),
	det_replace_nth(Temp, B, ItemA, Result).

:- pred should_change(int::in, int::in, float::in, float::in) is semidet.
should_change(Current, New, Temperature, N) :-
	New =< Current ;
	(
		Difference = float(Current - New),
		Exponent = Difference / Temperature,
		Prob = exp(Exponent),
		N < Prob
	).
	
:- pred get_product_of_departures(list(int)::in, list(field)::in, int::out) is det.
get_product_of_departures(Ticket, FieldOrder, Result) :-
	( if
		FieldOrder = [field(X, _, _) | Xs],
		Ticket = [Y | Ys]
	then
		( if
			prefix(X, "departure")
		then	
			get_product_of_departures(Ys, Xs, SubRes),
			Result = Y * SubRes
		else
			get_product_of_departures(Ys, Xs, Result)
		)
	else
		Result = 1
	).
