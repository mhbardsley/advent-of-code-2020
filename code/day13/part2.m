/* Advent of Code - Day 13, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, integer, solutions.

:- type euclidean
	--->	euclidean(
			val :: integer,
			mod :: integer
		).

:- type gcd
	--->	gcd(
			old_r :: integer,
			r :: integer,
			old_s :: integer,
			s :: integer,
			old_t :: integer,
			t :: integer
		).

:- pred get_earliest(list(string)::in, integer::out) is semidet.
get_earliest(InList, Result) :-
	InList = [_, Services],
	ServiceList = split_at_char(',', Services),
	to_pair(ServiceList, integer(0), Equations),
	solve_equations(Equations, Result).

:- pred solve_equations(list(euclidean)::in, integer::out) is det.
solve_equations(Equations, Result) :-
	( if
		Equations = [euclidean(Val, Mod)]
	then
		Result = Val mod Mod
	else if Equations = [X1, X2 | Xs]
	then
		solve_equations([NewEuc | Xs], Result),
		get_new_euc(X1, X2, NewEuc)
	else
		Result = integer(0)
	).


:- pred get_new_euc(euclidean::in, euclidean::in, euclidean::out) is det.
get_new_euc(euclidean(A1, N1), euclidean(A2, N2), Result) :-
	extended_euclidean(gcd(N1, N2, integer(1), integer(0), integer(0), integer(1)), M1, M2),
	NextValTemp = A1 * M2 * N2 + A2 * M1 * N1,
	NextMod = N1 * N2,
	NextVal = NextValTemp mod NextMod,
	Result = euclidean(NextVal, NextMod).

:- pred to_pair(list(string)::in, integer::in, list(euclidean)::out) is det.
to_pair(Services, Index, Result) :-
	( if
		Services = [X | Xs]
	then
		( if
			X = "x"
		then
			to_pair(Xs, Index + integer(1), Result)
		else
			IntX = integer(det_to_int(X)),
			Transformed = euclidean(-Index mod IntX, IntX),
			to_pair(Xs, Index + integer(1), Sub),
			Result = [Transformed | Sub]
		)
	else
		Result = []
	).

:- pred extended_euclidean(gcd::in, integer::out, integer::out) is det.
extended_euclidean(GCDIn, M1, M2) :-
	GCDIn = gcd(OldR, R, OldS, S, OldT, T),
	( if
		R > integer(0)
	then
		NewGCDIn = gcd(R, NewR, S, NewS, T, NewT),
		Quotient = OldR // R,
		NewR = OldR - (Quotient * R),
		NewS = OldS - (Quotient * S),
		NewT = OldT - (Quotient * T),
		extended_euclidean(NewGCDIn, M1, M2)
	else
		M1 = OldS,
		M2 = OldT
	).
		
main(!IO) :-
	process_stream(!IO, Initial),
	( if
	  	get_earliest(Initial, Result)
	then
		io.print(Result, !IO),
		io.nl(!IO)
	else
		io.print("Error encountered", !IO),
		io.nl(!IO)
	).
