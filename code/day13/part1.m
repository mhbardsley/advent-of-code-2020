/* Advent of Code - Day 13, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int, solutions.

:- pred get_min(list(int)::in, int::out) is det.
get_min(Remainders, Result) :-
	foldl(int.min, Remainders, max_int, Result).

:- pred get_multiplication(list(string)::in, int::out) is nondet.
get_multiplication(InList, Result) :-
	InList = [Timestamp, Services],
	to_int(Timestamp, TimestampInt),
	ServiceList = split_at_char(',', Services),
	filter((pred(X::in) is semidet :- X \= "x"), ServiceList, Remaining),
	ServiceInts = map(det_to_int, Remaining),
	Remainders = map((func(X) = X - TimestampInt mod X), ServiceInts),
	get_min(Remainders, MinimumWait),
	member_index0(MinimumWait, Remainders, Index),
	index0(ServiceInts, Index, Service),
	Service * MinimumWait = Result.

main(!IO) :-
	process_stream(!IO, Initial),
	solutions(get_multiplication(Initial), Result),
	io.print(Result, !IO),
	io.nl(!IO).
