/* Advent of Code - Day 22, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, int, string, require, set, bool, pair.

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	get_decks(StringRep, Deck1, Deck2),
	run_iterations(Deck1, Deck2, init, WinningDeck, _),
	calculate_value(WinningDeck, Result).

:- pred get_decks(list(string)::in, list(int)::out, list(int)::out) is det.
get_decks(StringRep, Deck1, Deck2) :-
/*	trace [io(!IO)]
	(
		io.print(StringRep, !IO),
		io.nl(!IO),
		io.nl(!IO)
	),
*/
	take_while(isnt_empty, StringRep, Part1, Part2),
	( if 
		Part1 = ["Player 1:" | D1String],
		Part2 = ["", "Player 2:" | D2String]
	then
		Deck1 = map(det_to_int, D1String),
		Deck2 = map(det_to_int, D2String)
	else
		error("get_decks")
	).

:- pred isnt_empty(string::in) is semidet.
isnt_empty(Line) :-
	Line \= "".

:- pred run_iterations(list(int)::in, list(int)::in, 
	set(pair(list(int), list(int)))::in, list(int)::out, bool::out) is det.
run_iterations(Deck1, Deck2, History, WinningDeck, Player1Win) :-
	( if 
	  	in_history(Deck1, Deck2, History)
	then
		WinningDeck = Deck1,
		Player1Win = yes
	else 
		insert(pair(Deck1, Deck2), History, NewHistory),
	(if
		Deck1 = [D1Top | D1Rest],
		Deck2 = [D2Top | D2Rest]
	then
		( if
		  	get_amount_of_rest(D1Top, D1Rest, SubD1),
			get_amount_of_rest(D2Top, D2Rest, SubD2)
		then
			run_iterations(SubD1, SubD2, init, _, IsP1),
			( if
			  	IsP1 = yes
			then
				NewDeck1 = D1Rest ++ [D1Top, D2Top],
				run_iterations(NewDeck1, D2Rest, 
					NewHistory, WinningDeck, Player1Win)
			else
				NewDeck2 = D2Rest ++ [D2Top, D1Top],
				run_iterations(D1Rest, NewDeck2, 
					NewHistory, WinningDeck, Player1Win)
			)
		else if
			D1Top > D2Top
		then
			NewDeck1 = D1Rest ++ [D1Top, D2Top],
			run_iterations(NewDeck1, D2Rest, NewHistory, 
				WinningDeck, Player1Win)
		else if
			D2Top > D1Top
		then
			NewDeck2 = D2Rest ++ [D2Top, D1Top],
			run_iterations(D1Rest, NewDeck2, NewHistory, 
				WinningDeck, Player1Win)
		else
			error("Encountered equal values (run_iterations)")
		)
	else if
		Deck1 = [_ | _]
	then
		WinningDeck = Deck1,
		Player1Win = yes
	else if
		Deck2 = [_ | _]
	then
		WinningDeck = Deck2,
		Player1Win = no
	else
		error("run_iterations")
	)).

:- pred in_history(list(int)::in, list(int)::in, 
	set(pair(list(int), list(int)))::in) is semidet.
in_history(Deck1, Deck2, History) :-
	NewPair = pair(Deck1, Deck2),
	member(NewPair, History).

:- pred get_amount_of_rest(int::in, list(int)::in, list(int)::out) is semidet.
get_amount_of_rest(AmountToTake, List, Result) :-
	take(AmountToTake, List, Result).

:- pred calculate_value(list(int)::in, int::out) is det.
calculate_value(WinningDeck, Result) :-
/*	trace [io(!IO)]
	(
		io.print(WinningDeck, !IO),
		io.nl(!IO),
		io.nl(!IO)
	),
*/
	foldr2(get_next_val, WinningDeck, 0, Result, 1, _).

:- pred get_next_val(int::in, int::in, int::out, int::in, int::out) is det.
get_next_val(CardVal, SumIn, SumOut, MultIn, MultOut) :-
	SumOut = SumIn + MultIn * CardVal,
	MultOut = MultIn + 1.
