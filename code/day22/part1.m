/* Advent of Code - Day 22, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, int, string, require.

main(!IO) :-
	process_stream(!IO, StringRep),
	io.print(Result, !IO),
	io.nl(!IO),
	get_decks(StringRep, Deck1, Deck2),
	run_iterations(Deck1, Deck2, WinningDeck),
	calculate_value(WinningDeck, Result).

:- pred get_decks(list(string)::in, list(int)::out, list(int)::out) is det.
get_decks(StringRep, Deck1, Deck2) :-
/*	trace [io(!IO)]
	(
		io.print(StringRep, !IO),
		io.nl(!IO),
		io.nl(!IO)
	),
*/
	take_while(isnt_empty, StringRep, Part1, Part2),
	( if 
		Part1 = ["Player 1:" | D1String],
		Part2 = ["", "Player 2:" | D2String]
	then
		Deck1 = map(det_to_int, D1String),
		Deck2 = map(det_to_int, D2String)
	else
		error("get_decks")
	).

:- pred isnt_empty(string::in) is semidet.
isnt_empty(Line) :-
	Line \= "".

:- pred run_iterations(list(int)::in, list(int)::in, list(int)::out) is det.
run_iterations(Deck1, Deck2, WinningDeck) :-
	( if
		Deck1 = [D1Top | D1Rest],
		Deck2 = [D2Top | D2Rest]
	then
		( if
			D1Top > D2Top
		then
			NewDeck1 = D1Rest ++ [D1Top, D2Top],
			run_iterations(NewDeck1, D2Rest, WinningDeck)
		else if
			D2Top > D1Top
		then
			NewDeck2 = D2Rest ++ [D2Top, D1Top],
			run_iterations(NewDeck2, D1Rest, WinningDeck)
		else
			error("Encountered equal values (run_iterations)")
		)
	else if
		Deck1 = [_ | _]
	then
		WinningDeck = Deck1
	else if
		Deck2 = [_ | _]
	then
		WinningDeck = Deck2
	else
		error("run_iterations")
	).

:- pred calculate_value(list(int)::in, int::out) is det.
calculate_value(WinningDeck, Result) :-
/*	trace [io(!IO)]
	(
		io.print(WinningDeck, !IO),
		io.nl(!IO),
		io.nl(!IO)
	),
*/
	foldr2(get_next_val, WinningDeck, 0, Result, 1, _).

:- pred get_next_val(int::in, int::in, int::out, int::in, int::out) is det.
get_next_val(CardVal, SumIn, SumOut, MultIn, MultOut) :-
	SumOut = SumIn + MultIn * CardVal,
	MultOut = MultIn + 1.
