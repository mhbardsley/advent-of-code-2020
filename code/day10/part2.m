/* Advent of Code - Day 10, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int, solutions.

:- pred add_bounds(list(int)::in, list(int)::out) is det.
add_bounds(InList, Result) :-
	cons(0, InList, Temp),
	det_last(Temp, Val),
	ToAppend = Val + 3,
	append(Temp, [ToAppend], Result).

:- pred create_differences(list(int)::in, list(int)::out) is det.
create_differences(InList, Result) :-
	( if
	  	InList = [First, Second | Rest]
	then
		Difference = Second - First,
		NewList = [Second | Rest],
		create_differences(NewList, SubRest),
		Result = [Difference | SubRest]
	else
		Result = []
	).

:- pred sub_difference(list(int)::in, list(int)::out) is multi.
sub_difference(InList, Result) :-
	Result = InList ;
	some [X, Y]
	(
		member_index0(X, InList, I),
		J = I + 1,
		member_index0(Y, InList, J),
		Tot = X + Y,
		Tot =< 3,
		split_list(I, InList, Start, Mid),
		split_list(2, Mid, _, SemiEnd),
		cons(Tot, SemiEnd, End),
		append(Start, End, Res),
		(Result = Res ; sub_difference(Res, Result))
	).

:- pred split_3s(list(int)::in, list(int)::in, list(list(int))::out) is det.
split_3s(InList, Current, Result) :-
	( if
		InList = [X | Rest]
	then
		( if
			X = 3
		then
			split_3s(Rest, [], OtherResult),
			Result = [Current | OtherResult]
		else
			NewCurrent = [X | Current],
			split_3s(Rest, NewCurrent, Result)
		)
	else
		Result = [Current]
	).

:- pred get_results(list(int)::in, int::out) is det.
get_results(InList, Result) :-
	solutions(sub_difference(InList), Sols),
	length(Sols, Result).

main(!IO) :-
	process_stream(!IO, Strings),
	filter_map(to_int, Strings, Ints),
	sort(Ints, WithoutBounds),
	add_bounds(WithoutBounds, WithBounds),
	create_differences(WithBounds, Differences),
	split_3s(Differences, [], Splits),
	map(get_results, Splits, Factors),
	Result = foldl(times, Factors, 1),
	io.print(Result, !IO),
	io.nl(!IO).
