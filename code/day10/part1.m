/* Advent of Code - Day 10, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int.

:- pred add_bounds(list(int)::in, list(int)::out) is det.
add_bounds(InList, Result) :-
	cons(0, InList, Temp),
	det_last(Temp, Val),
	ToAppend = Val + 3,
	append(Temp, [ToAppend], Result).

:- pred create_differences(list(int)::in, list(int)::out) is det.
create_differences(InList, Result) :-
	( if
	  	InList = [First, Second | Rest]
	then
		Difference = Second - First,
		NewList = [Second | Rest],
		create_differences(NewList, SubRest),
		Result = [Difference | SubRest]
	else
		Result = []
	).

:- pred get_three_ones(list(int)::in, int::out, int::out) is det.
get_three_ones(Differences, Ones, Threes) :-
	( if
	  	Differences = [X | Rest]
	then
		get_three_ones(Rest, OtherOnes, OtherThrees),
		( if
		  	X = 1
		then
			Ones = OtherOnes + 1,
			Threes = OtherThrees
		else
			Threes = OtherThrees + 1,
			Ones = OtherOnes
		)
	else
		Ones = 0,
		Threes = 0
	).

main(!IO) :-
	process_stream(!IO, Strings),
	filter_map(to_int, Strings, Ints),
	sort(Ints, FirstJolts),
	add_bounds(FirstJolts, TotalJolts),
	create_differences(TotalJolts, Differences),
	get_three_ones(Differences, Ones, Threes),
	Result = Ones * Threes,
	io.print(Result, !IO),
	io.nl(!IO).
