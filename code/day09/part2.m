/* Advent of Code - Day 9, Part 2
* Author: Matthew Bardsley
*/

:- module part2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int, solutions.

:- pred check_window(list(int)::in, int::in, int::in, int::out) is det.
check_window(Numbers, Start, Offset, Result) :-
	% check that the window is valid
	length(Numbers, Length),
	End = Start + Offset,
	Invalid = End + 1,
	( if
	  	Invalid >= Length
	then
		Result = -1
	else
		det_split_list(Start, Numbers, _, WindowStart),
		det_split_list(Offset, WindowStart, Window, _),
		det_index0(Numbers, End, NumberToCheck),
		( if
			some [X, Y]
			(
		 		member(X, Window),
				member(Y, Window),
				X + Y = NumberToCheck
			)
		then
			NextStart = Start + 1,
			check_window(Numbers, NextStart, Offset, Result)
		else
			Result = NumberToCheck
		)
	).

:- pred get_first(list(int)::in, int::out) is det.
get_first(Numbers, Result) :-
	check_window(Numbers, 0, 25, Result).


:- pred powerset(list(int)::in, list(int)::out) is nondet.
powerset(Numbers, Result) :-
	length(Numbers, Length),
	some [Start, Offset]
	(
		member(Start, 0 .. Length),
		member(Offset, 2 .. Length),
		End = Start + Offset,
		End =< Length,
		split_list(Start, Numbers, _, WindowStart),
		split_list(Offset, WindowStart, Result, _)
	).

:- pred reduce_list(int::in, int::in) is semidet.
reduce_list(FirstNumber, Other) :-
	Other \= FirstNumber.

:- pred is_weakness(int::in, list(int)::in, int::out) is semidet.
is_weakness(Target, Subset, Result) :-
	Sum = foldl(int.plus, Subset, 0),
	Sum = Target,
	FirstVal = foldl(int.min, Subset, max_int),
	LastVal = foldl(int.max, Subset, min_int),
	Result = FirstVal + LastVal.

main(!IO) :-
	process_stream(!IO, StringNumbers),
	filter_map(to_int, StringNumbers, Numbers),
	get_first(Numbers, FirstNumber),
	take_while(reduce_list(FirstNumber), Numbers, Subset),
	solutions(powerset(Subset), Powerset),
	filter_map(is_weakness(FirstNumber), Powerset, Result),
	io.print(Result, !IO),
	io.nl(!IO).
