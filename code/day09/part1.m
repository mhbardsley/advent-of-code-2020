/* Advent of Code - Day 9, Part 1
* Author: Matthew Bardsley
*/

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, list, string, int.

:- pred check_window(list(int)::in, int::in, int::in, int::out) is det.
check_window(Numbers, Start, Offset, Result) :-
	% check that the window is valid
	length(Numbers, Length),
	End = Start + Offset,
	Invalid = End + 1,
	( if
	  	Invalid >= Length
	then
		Result = -1
	else
		det_split_list(Start, Numbers, _, WindowStart),
		det_split_list(Offset, WindowStart, Window, _),
		det_index0(Numbers, End, NumberToCheck),
		( if
			some [X, Y]
			(
		 		member(X, Window),
				member(Y, Window),
				X + Y = NumberToCheck
			)
		then
			NextStart = Start + 1,
			check_window(Numbers, NextStart, Offset, Result)
		else
			Result = NumberToCheck
		)
	).

:- pred get_first(list(int)::in, int::out) is det.
get_first(Numbers, Result) :-
	check_window(Numbers, 0, 25, Result).

main(!IO) :-
	process_stream(!IO, StringNumbers),
	filter_map(to_int, StringNumbers, Numbers),
	get_first(Numbers, Result),
	io.print(Result, !IO),
	io.nl(!IO).
