/* Advent of Code - Day 11, Part 1
 * Author: Matthew Bardsley
 */

:- module part1.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module input_processor, int, list, string, char, solutions.

:- pred convert_char(char::in, list(char)::in, char::out) is det.
convert_char(InChar, Adjacent, NewChar) :-
	( if
		InChar = 'L', 
		not (some [X] 
		(
	 		member(X, Adjacent), X = '#'
		))	
	then
		NewChar = '#'
	else if
		InChar = '#',
		(
		 filter(pred(X::in) is semidet :- X = '#', Adjacent, Occupied),
			length(Occupied, NoOccupied),
			NoOccupied >= 4
		)
	then
		NewChar = 'L'
	else
		NewChar = InChar
	).

:- pred get_adjacent(int::in, int::in, list(list(char))::in, list(int)::out) is nondet.
get_adjacent(RowNo, ColumnNo, Mappings, Adjacent) :-
	length(Mappings, NoOfRows),
	FinalRow = NoOfRows - 1,
	index0(Mappings, RowNo, Row),
	length(Row, NoOfColumns),
	FinalColumn = NoOfColumns - 1,
	some [I, J]
	(
	 	member(I, max(0, RowNo-1) .. min(FinalRow, RowNo+1)),
		member(J, max(0, ColumnNo-1) .. min(FinalColumn, ColumnNo+1)),
		not (I = RowNo, J = ColumnNo),
		Adjacent = [I, J]
	).

:- pred coords_to_char(list(list(char))::in, list(int)::in, char::out) is semidet.
coords_to_char(Mappings, Coords, Result) :-
	Coords = [X, Y],
	index0(Mappings, X, Row),
	index0(Row, Y, Result).

:- pred get_adjacents(int::in, int::in, list(list(char))::in, list(char)::out) is det.
get_adjacents(RowNo, ColumnNo, Mappings, Adjacents) :-
	solutions(get_adjacent(RowNo, ColumnNo, Mappings), Coords),
	filter_map(coords_to_char(Mappings), Coords, Adjacents).

:- pred convert_row(int::in, int::in, list(char)::in, list(list(char))::in, list(char)::out) is det.
convert_row(RowNo, ColumnNo, Row, Mappings, NewRow) :-
	( if
	  	Row = [X | Xs]
	then
		NextCol = ColumnNo + 1,
		convert_row(RowNo, NextCol, Xs, Mappings, Rest),
		NewRow = [NewX | Rest],
		get_adjacents(RowNo, ColumnNo, Mappings, Adjacents),
		convert_char(X, Adjacents, NewX)
	else
		NewRow = []
	).

:- pred convert_mapping(int::in, list(list(char))::in, list(list(char))::in, list(list(char))::out) is det.
convert_mapping(RowNo, RemainingRows, Mappings, NewMappings) :-
	( if
	  	RemainingRows = [X | Xs]
	then
		NextRowNo = RowNo + 1,
		convert_mapping(NextRowNo, Xs, Mappings, Rest),
		NewMappings = [NewRow | Rest],
		convert_row(RowNo, 0, X, Mappings, NewRow)
	else
		NewMappings = []
	).

:- pred make_chars(list(string)::in, list(list(char))::out) is det.
make_chars(InList, Result) :-
	( if
		InList = [X | Xs]
	then
		to_char_list(X, CharList),
		make_chars(Xs, OtherResult),
		Result = [CharList | OtherResult]
	else
		Result = []
	).

:- pred make_str(list(list(char))::in, list(string)::out) is det.
make_str(InList, Result) :-
	( if
		InList = [X | Xs]
	then
		from_char_list(X, CharList),
		make_str(Xs, OtherResult),
		Result = [CharList | OtherResult]
	else
		Result = []
	).

:- pred get_stability(list(list(char))::in, list(list(char))::out) is det.
get_stability(InList, Result) :-
	convert_mapping(0, InList, InList, Temp),
	( if
	  	InList = Temp
	then
		Result = Temp
	else
		get_stability(Temp, Result)
	).

main(!IO) :-
	process_stream(!IO, Initial),
	make_chars(Initial, Mappings),
	get_stability(Mappings, T1),
	condense(T1, T2),
	filter(pred(X::in) is semidet :- X = '#', T2, T3),
	length(T3, Result),
	io.print(Result, !IO),
	io.nl(!IO).
